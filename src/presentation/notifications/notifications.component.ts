import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {Observable, of} from "rxjs";
import {loadNotifications} from "../../store/notifications/notifications.actions";
import {selectNotificationsState} from "../../store/notifications/notifications.selectors";
import {NotificationsState} from "../../store/notifications/notifications.reducer";
import {NotificationContext} from "../../domain/model/notifications.model";
import {IMqttMessage, MqttService} from "ngx-mqtt";

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
  state: Observable<NotificationsState> = of();

  constructor(private _mqttService: MqttService, private store: Store) {

  }

  ngOnInit(): void {
    this.getNotifications();
    this._mqttService.observe('/notification/#').subscribe((message: IMqttMessage) => {
      this.getNotifications();

    });
  }

  getNotifications() {
    this.store.dispatch(loadNotifications())
    this.state = this.store.select(selectNotificationsState);
    this.state.subscribe(value => {
      console.log(value.Notifications)
    })
  }

  protected readonly Notification = Notification;
  protected readonly NotificationContext = NotificationContext;
}
