import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from "rxjs";
import {LocalStorageService} from "../../utils/localstorage";

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private localStorage: LocalStorageService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (localStorage.getItem('token')) {
      return true;
    }
    // not logged in so redirect to login page with the return url
    this.router.navigateByUrl('/login');
    return false;
  }
}
