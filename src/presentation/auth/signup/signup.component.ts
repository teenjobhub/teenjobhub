import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {UserModel} from "../../../domain/model/user.model";
import {RoleModel} from "../../../domain/model/role.model";
import {select, Store} from "@ngrx/store";
import {signUp} from "../../../store/auth/signup/signup.actions";
import {Observable} from "rxjs";
import {selectUser} from "../../../store/auth/signup/signup.selectors";
import {login} from "../../../store/auth/login/login.actions";
import {selectLoginState} from "../../../store/auth/login/login.selectors";
import {LocalStorageService} from "../../../utils/localstorage";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {
  username: string = '';
  password: string = '';
  email: string = '';
  role: RoleModel = {
    id: "",
    name: ""
  };
  loading: boolean = false; // Add a boolean property to track login state

  userModel: Observable<UserModel> | undefined

  constructor(private router: Router, private store: Store, private localStorageService: LocalStorageService) {
  }

  onSignUp(): void {
    this.loading = true;
    const signUpParams = {
      email: this.email,
      password: this.password,
      username: this.username,
      role: this.role
    };
    this.store.dispatch(signUp(signUpParams))
    this.userModel = this.store.pipe(select(selectUser));
    this.userModel.subscribe((user) => {
      this.localStorageService.saveData('user', user)
      if (user.email.length > 0) {
        this.loginNewUser()
      }
    })

  }

  loginNewUser() {
    this.store.dispatch(login({password: this.password, username: this.username,}));
    this.store.pipe(select(selectLoginState)).subscribe(
      (login) => {
        if (login.success) {
          if (login.tokenModel) {
            this.localStorageService.removeData('token')
            this.localStorageService.saveData('token', login.tokenModel)
            this.loading = false;
            this.router.navigate(['/profile-setup']);
          }
        } else if (login.error) {
          (login.error)
        }
      }
    );
  }

}
