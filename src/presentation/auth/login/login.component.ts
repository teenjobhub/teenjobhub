import {Component} from '@angular/core';
import {UserImplementationRepository} from "../../../domain/repositories/user/user-implementation.repository";
import {Router} from "@angular/router";
import {select, Store} from '@ngrx/store';
import {login} from "../../../store/auth/login/login.actions";
import {selectLoginState} from "../../../store/auth/login/login.selectors";
import {LocalStorageService} from "../../../utils/localstorage";
import {selectUserProfileState} from "../../../store/user-profile/user-profile.selectors";
import {getUserProfile} from "../../../store/user-profile/user-profile.actions";
import {JwtDecodeService} from "../../../utils/jwt.decoder";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserImplementationRepository],
})
export class LoginComponent {
  username: string = '';  // Declare username property
  password: string = '';  // Declare password property
  isLoggingIn: boolean = false; // Add a boolean property to track login state

  constructor(private store: Store,
              private router: Router,
              private localStorageService: LocalStorageService,
              private jwtDecode: JwtDecodeService) {

  }

  onLogin(): void {
    this.store.dispatch(login({password: this.password, username: this.username,}));
    this.store.pipe(select(selectLoginState)).subscribe(
      (login) => {
        this.isLoggingIn = true;
        if (login.success) {
          if (login.tokenModel) {
            this.localStorageService.removeData('token')
            this.localStorageService.saveData('token', login.tokenModel)
            this.store.dispatch(getUserProfile({userID: this.jwtDecode.getUserIDFromToken()}))
            this.store.pipe(
              select(selectUserProfileState)
            ).subscribe(
              (profile) => {
                if (profile.success) {
                  if (profile.userProfile == null) {
                    this.isLoggingIn = false;
                    this.router.navigate(['/profile-setup']).then(value => {
                    })
                  } else {
                    this.localStorageService.saveData('userProfile', profile.userProfile)
                    this.router.navigate(['/main/home']).then(value => {
                    })
                  }

                }
              }
            )
          }
        } else if (login.error) {
        }
      }
    );

  }
}
