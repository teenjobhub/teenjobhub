import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {getPostByUser, myApplications} from "../../store/post/post.actions";
import {selectJobPostByUserState, selectMyApplications} from "../../store/post/post.selectors";
import {Observable, of} from "rxjs";
import {GetJobsByUserState} from "../../store/post/post.reducer";
import {DatePipe} from "@angular/common";
import {MatDialog} from "@angular/material/dialog";
import {HireDialogComponent} from "../components/hire-dialog/hire-dialog.component";
import {RatingComponent} from "../components/rating/rating.component";
import {JobPostModel} from "../../domain/model/job-post.model";

@Component({
  selector: 'app-jobs-management',
  templateUrl: './jobs-management.component.html',
  styleUrls: ['./jobs-management.component.css']
})
export class JobsManagementComponent implements OnInit {

  state: Observable<GetJobsByUserState> = of();
  closed :boolean = false;
  applications :boolean = false;

  myApplicationList:Observable<JobPostModel[]> = of()

  constructor(private store: Store, public datePipe: DatePipe, private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.findJobPosts(false)
  }

  openDialog(firstName: string, lastName: string, title: string, jobPostID: string, userProfileID: string): void {
    const dialogRef = this.dialog.open(HireDialogComponent, {
      height: 'fit',
      width: '40%',
      data: {
        "firstName": firstName,
        "lastName": lastName,
        "title": title,
        "jobPostID": jobPostID,
        "userProfileID": userProfileID,
      }, // Pass your parameter here
    }).afterClosed().subscribe(
      value => {
        this.findJobPosts(false)

      }
    );
  }

  findJobPosts(closed:boolean){
    this.applications=false;
    this.closed=closed;
    this.store.dispatch(getPostByUser(
      {closed: closed}
    ))
    this.state = this.store.select(selectJobPostByUserState);
    this.state.subscribe(value => {
    })
  }

  myApplications(){
    this.applications=true;
    this.store.dispatch(myApplications(
    ))
    this.store.select(selectMyApplications).subscribe(
      value => {
        if (value.length>0){
          this.myApplicationList=of(value);

        }
      }
    );
  }



  openRateDialog(jobPostID:string,title:string,hiredUserName:string) {
    this.dialog.open(RatingComponent,{
      height: 'fit',
      width: '40%',
      data:{
        "jobPostID": jobPostID,
        "title": title,
        "hiredUserName":hiredUserName
      }
    })
  }
}
