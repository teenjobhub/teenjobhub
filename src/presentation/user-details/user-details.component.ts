import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {ActivatedRoute} from "@angular/router";
import {profileById} from "../../store/user-profile/user-profile.actions";
import {selectUserProfileByIdState} from "../../store/user-profile/user-profile.selectors";
import {Observable, of} from "rxjs";
import {ProfileByIdState} from "../../store/user-profile/user-profile.reducer";
import {addConnection} from "../../store/connections/connections.actions";

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  userProfileState: Observable<ProfileByIdState> = of()
  userProfileId: string = ""

  constructor(private store: Store, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
          this.userProfileId = params['userProfileId'];
          this.store.dispatch(profileById({userProfileId: params['userProfileId']}))
          this.userProfileState = this.store.select(selectUserProfileByIdState)
        }
      );
  }

  addUser() {
    this.store.dispatch(addConnection({destinationUserID:this.userProfileId}))
  }

}
