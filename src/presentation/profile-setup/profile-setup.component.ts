import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {SkillModel} from "../../domain/model/skill.model";
import {Observable} from "rxjs";
import {select, Store} from '@ngrx/store';
import {loadSkills} from "../../store/skill/skills.actions";
import {selectSkills} from "../../store/skill/skills.selectors";
import {loadOccupations} from "../../store/occupation/occupation.actions";
import {OccupationModel} from "../../domain/model/occupation.model";
import {selectOccupations} from "../../store/occupation/occupation.selectors";
import * as isoCountries from 'i18n-iso-countries';
import * as en from 'i18n-iso-countries/langs/en.json';
import {UserProfileModel} from "../../domain/model/user-profile.model";
import {addUserProfile} from "../../store/user-profile/user-profile.actions";
import {selectUserProfileState} from "../../store/user-profile/user-profile.selectors";
import {Router} from "@angular/router";
import {UpdateUserImageUseCase} from "../../domain/usecases/user/update-user-image.usecase";
import {LocalStorageService} from "../../utils/localstorage";
import SignaturePad from "signature_pad";
import {UpdateUserSignatureUsecase} from "../../domain/usecases/user/update-user-signature.usecase";

@Component({
  selector: 'app-profile-setup',
  templateUrl: './profile-setup.component.html',
  styleUrls: ['./profile-setup.component.css']
})
export class ProfileSetupComponent implements OnInit {
  userPhoto: string | ArrayBuffer | null = null;
  skills$: Observable<SkillModel[]> | undefined;
  occupations$: Observable<OccupationModel[]> | undefined;
  selectedOccupation: OccupationModel = {
    id: '',
    name: ''
  };
  countries: { alpha2: string; name: string }[] = [];

  selectedCountry: { alpha2: string; name: string } = {alpha2: "", name: ""};
  selectedSkillList: SkillModel[] = [];

  firstName: string = ""
  lastName: string = ""
  phoneNumber: string = ""
  state: string = ""
  userImage: File = new File([], 'dummy.jpg')
  @ViewChild("canvas", {static: true}) canvas: ElementRef | undefined;
  sig: SignaturePad | undefined;

  constructor(private store: Store, private router: Router, private updateImage: UpdateUserImageUseCase, private updateUserSignatureUsecase: UpdateUserSignatureUsecase, private localStorage: LocalStorageService) {
    isoCountries.registerLocale(en);
    this.countries = Object.entries(isoCountries.getNames('en'))
      .map(([alpha2, name]) => ({alpha2, name}));
    this.selectedCountry = this.countries[0];

  }

  ngOnInit() {
    this.sig = new SignaturePad(this.canvas!.nativeElement);

    this.store.dispatch(loadSkills());
    this.skills$ = this.store.pipe(select(selectSkills));
    this.store.dispatch(loadOccupations());
    this.occupations$ = this.store.pipe(select(selectOccupations));
    this.occupations$.subscribe((occupations) => {
      if (occupations && occupations.length > 0) {
        this.selectedOccupation = occupations[0];
      }
    })
  }

  updateSelectedOccupation(event: OccupationModel) {
    this.selectedOccupation = event;
  }

  onFileSelected(event: any) {
    const file = event.target.files[0];
    this.userImage = file
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        this.userPhoto = reader.result;
      };
      reader.readAsDataURL(file);
    }
  }


  toggleSelection(skill: SkillModel) {
    const index = this.selectedSkillList.findIndex(selectedSkill => selectedSkill.id === skill.id);
    if (index !== -1) {
      this.selectedSkillList.splice(index, 1);
    } else {
      this.selectedSkillList.push(skill);
    }
  }

  isSelected(skill: SkillModel): boolean {
    return this.selectedSkillList.some(selectedSkill => selectedSkill.id === skill.id);
  }

  onSubmitUserProfile() {
    const userProfile: UserProfileModel = {
      id: "",
      firstName: this.firstName,
      lastName: this.lastName,
      phoneNumber: this.phoneNumber,
      country: this.selectedCountry.name,
      governance: this.state,
      user: {
        id: '',
        username: '',
        email: '',
        token: ''
      },
      occupation: this.selectedOccupation,
      skills: this.selectedSkillList,
      userImage: ""
    }
    this.store.dispatch(addUserProfile({userProfile,}));
    this.store.pipe(select(selectUserProfileState)).subscribe(value => {
      if (!value.loading && value.success) {
        this.updateImage.execute({userImage: this.userImage, id: value.userProfile.id}).subscribe(value => {
          if (value) {
            this.updateUserSignatureUsecase.execute({userImage: this.exportCanvasAsPNG(), id: ""},).subscribe(
              value1 => {
                if(value1.firstName.length>0){
                  this.router.navigate(['/main/home'])
                  this.localStorage.saveData('userProfile', value1)
                }

              }
            )
          }
        })
      }
    });
  }

  exportCanvasAsPNG() {


    const canvasEl = this.canvas!.nativeElement;
    const dataUrl = canvasEl.toDataURL('image/png');

    const binary = atob(dataUrl.split(',')[1]);
    const array = [];

    for (let i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }

    const blob = new Blob([new Uint8Array(array)], {type: 'image/png'});

    return new File([blob], 'signature.png', {type: 'image/png'});
  }

}
