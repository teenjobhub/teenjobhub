import {Component} from '@angular/core';
import {UserProfileModel} from "../../../domain/model/user-profile.model";
import {LocalStorageService} from "../../../utils/localstorage";

@Component({
  selector: 'app-user-profile-side-bar',
  templateUrl: './user-profile-side-bar.component.html',
  styleUrls: ['./user-profile-side-bar.component.css']
})
export class UserProfileSideBarComponent {

  public userProfile: UserProfileModel;

  constructor(private localStorage: LocalStorageService) {
    this.userProfile = localStorage.getData('userProfile')
  }

}
