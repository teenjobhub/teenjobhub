import {ComponentFixture, TestBed} from '@angular/core/testing';

import {JobSpaceDialogComponent} from './job-space-dialog.component';

describe('JobSpaceDialogComponent', () => {
  let component: JobSpaceDialogComponent;
  let fixture: ComponentFixture<JobSpaceDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [JobSpaceDialogComponent]
    });
    fixture = TestBed.createComponent(JobSpaceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
