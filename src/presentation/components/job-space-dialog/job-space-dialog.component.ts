import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";
import {select, Store} from "@ngrx/store";
import {JobSpaceModel} from "../../../domain/model/job-space.model";
import {LocalStorageService} from "../../../utils/localstorage";
import {UserProfileModel} from "../../../domain/model/user-profile.model";
import {getUserConnections} from "../../../store/connections/connections.actions";
import {selectUserConnections} from "../../../store/connections/connections.selectors";
import {Observable, of} from "rxjs";
import {ConnectionRequestModel} from "../../../domain/model/connection-request.model";
import {createJobSpaceRequests} from "../../../store/jobspace/jobspace.actions";
import {selectCreateJobSpaceState} from "../../../store/jobspace/jobspace.selectors";

@Component({
  selector: 'app-job-space-dialog',
  templateUrl: './job-space-dialog.component.html',
  styleUrls: ['./job-space-dialog.component.css']
})
export class JobSpaceDialogComponent implements OnInit {
  public jobSpace: JobSpaceModel = {
    name: '',
    id: '',
    admin: this.localStorage.getData('userProfile'),
    staff: []
  }
  public userProfile: UserProfileModel;

  public userConnections: Observable<ConnectionRequestModel[]> = of([]);
  public selectedUserConnections: UserProfileModel[] = [];

  public loadingConnections: boolean = false

  constructor(public dialogRef: MatDialogRef<JobSpaceDialogComponent>, private store: Store, public localStorage: LocalStorageService) {
    this.userProfile = localStorage.getData('userProfile')

  }

  closeDialog(): void {
    this.dialogRef.close();
  }


  addJobSpace() {

    this.selectedUserConnections.forEach(value => {
      this.jobSpace.staff.push({
        pending: true,
        userProfile: value
      })
    })

    this.store.dispatch(createJobSpaceRequests({jobSpaceModel: this.jobSpace}))
    this.store.pipe(select(selectCreateJobSpaceState)).subscribe(
      value => {
        if (value.success) {
          this.closeDialog()
        }
      }
    )
  }

  toggleSelection(user: UserProfileModel) {
    const index = this.selectedUserConnections.findIndex(selectedProfile => selectedProfile.id === user.id);
    if (index !== -1) {
      this.selectedUserConnections.splice(index, 1);
    } else {
      this.selectedUserConnections.push(user);
    }
  }

  isSelected(user: UserProfileModel): boolean {
    return this.selectedUserConnections.some(selectedUser => selectedUser.id === user.id);
  }

  ngOnInit(): void {
    this.loadingConnections = true;
    this.store.dispatch(getUserConnections())
    this.userConnections = this.store.pipe(select(selectUserConnections));
    this.userConnections.subscribe(value => {
      if (value.length > 0) {
        this.loadingConnections = false;
      }
    })
  }
}
