import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngrx/store";
import {hire} from "../../../store/post/post.actions";
import {selectHiredState} from "../../../store/post/post.selectors";
import {Observable, of} from "rxjs";
import {HiredState} from "../../../store/post/post.reducer";
import {PdfDownloadService} from "../../../utils/pdf.service";

@Component({
  selector: 'app-hire-dialog',
  templateUrl: './hire-dialog.component.html',
  styleUrls: ['./hire-dialog.component.css']
})
export class HireDialogComponent {
  state: Observable<HiredState> = of();
  pdfUrl: string = "";

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private store: Store,
              private pdfDownloadService: PdfDownloadService,public dialogRef: MatDialogRef<HireDialogComponent>,) {

  }

  hireForJob() {
    this.store.dispatch(hire({jobPostID: this.data.jobPostID, userProfileID: this.data.userProfileID}))
    this.state = this.store.select(selectHiredState);
    this.state.subscribe(value => {
      this.pdfUrl = value.hired
    })
  }
  closeDialog(): void {
    this.dialogRef.close();
  }
  downloadPdf() {
    this.pdfDownloadService.downloadPdf(`http://localhost:8092/file/${this.pdfUrl}`).subscribe(response => {
      const blob = new Blob([response], {type: 'application/pdf'});
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      a.href = url;
      a.download = 'contract.pdf';
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
      window.URL.revokeObjectURL(url);
    });
    this.closeDialog()
  }
}

