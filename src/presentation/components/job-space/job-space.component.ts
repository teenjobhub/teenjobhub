import {Component, OnInit} from '@angular/core';
import {Observable, of} from "rxjs";
import {select, Store} from "@ngrx/store";
import {JobSpaceModel} from "../../../domain/model/job-space.model";
import {
  selectAcceptOrDenyJobSpaceState,
  selectGetUserJobSpacesState,
  selectJobSpaceModel
} from "../../../store/jobspace/jobspace.selectors";
import {
  acceptOrDenyInvitationRequests,
  getJobSpacesPendingRequests,
  getUserJobSpacesRequests
} from "../../../store/jobspace/jobspace.actions";
import {LocalStorageService} from "../../../utils/localstorage";
import {MatDialog} from "@angular/material/dialog";
import {JobSpaceDialogComponent} from "../job-space-dialog/job-space-dialog.component";

@Component({
  selector: 'app-job-space',
  templateUrl: './job-space.component.html',
  styleUrls: ['./job-space.component.css']
})
export class JobSpaceComponent implements OnInit {
  pendingInvitations: Observable<JobSpaceModel[]> | undefined;
  userJobSpaces: Observable<JobSpaceModel[]> = of([]);

  loadingPendingInvitations: boolean = false;
  loadingUserJobSpaces: boolean = false;
  accepted: boolean = false;

  constructor(private store: Store, private localStorage: LocalStorageService, public dialog: MatDialog,) {

  }

  ngOnInit() {
    this.getUserPendingJobSpacesRequest();
    this.getUserJobSpaces();
  }

  onAcceptJobSpaceRequest(jobSpaceID: string, userDecision: boolean) {
    const userProfileID = this.localStorage.getData('userProfile').id;
    this.store.dispatch(acceptOrDenyInvitationRequests({
        jobSpaceID,
        userProfileID,
        userDecision
      }
    ));
    this.store.pipe(select(selectAcceptOrDenyJobSpaceState)).subscribe((accepted) => {
      if (accepted.success&& !this.accepted) {
        this.accepted = accepted.success;
        this.getUserPendingJobSpacesRequest()
        this.getUserJobSpaces()
      }
    })
  }

  getUserPendingJobSpacesRequest() {
    this.loadingPendingInvitations = true;
    this.store.dispatch(getJobSpacesPendingRequests());
    this.pendingInvitations = this.store.pipe(select(selectJobSpaceModel));
    this.pendingInvitations.subscribe((pendingConnections) => {
      if (pendingConnections.length > 0) {
        this.loadingPendingInvitations = false;

      }
    });
  }

  getUserJobSpaces() {
    this.loadingPendingInvitations = true;
    this.store.dispatch(getUserJobSpacesRequests());
    this.store.pipe(select(selectGetUserJobSpacesState)).subscribe((jobspaces) => {
      if (jobspaces.success) {
        this.loadingPendingInvitations = false;
        this.userJobSpaces = of(jobspaces.jobSpaces);
      }
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(JobSpaceDialogComponent, {
      height: 'fit',
      width: 'fit',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getUserJobSpaces()
    });
  }


}
