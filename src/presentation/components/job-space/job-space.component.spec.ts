import {ComponentFixture, TestBed} from '@angular/core/testing';

import {JobSpaceComponent} from './job-space.component';

describe('JobSpaceComponent', () => {
  let component: JobSpaceComponent;
  let fixture: ComponentFixture<JobSpaceComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [JobSpaceComponent]
    });
    fixture = TestBed.createComponent(JobSpaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
