import {Component} from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";
import {AddPostModel} from "../../../domain/model/add-post.model";
import {select, Store} from "@ngrx/store";
import {selectPostState} from "../../../store/post/post.selectors";
import {addPost} from "../../../store/post/post.actions";
import {Observable, of} from "rxjs";
import {UserProfileModel} from "../../../domain/model/user-profile.model";
import {LocalStorageService} from "../../../utils/localstorage";

@Component({
  selector: 'app-setting-dialog',
  templateUrl: './post-dialog.component.html',
  styleUrls: ['./post-dialog.component.css']
})
export class PostDialogComponent {
  selectedImage: boolean = false;
  displayedImage: string | ArrayBuffer = '';
  loading$: Observable<boolean> = of(false);
  public postModel: AddPostModel = {
    jobPostDescription: '',
    postImage: new File([], 'dummy.jpg'),
    jobPostTitle: ''
  }
  public userProfile: UserProfileModel;


  constructor(public dialogRef: MatDialogRef<PostDialogComponent>, private store: Store, public localStorage: LocalStorageService) {
    this.userProfile = localStorage.getData('userProfile')

  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  async addPost() {
    await new Promise<void>(resolve => {
      const subscription = this.store.pipe(select(selectPostState)).subscribe(value => {
        if (!value.loading) {
          this.closeDialog();
          resolve();
        } else if (value.error) {
          resolve();
        } else if (value.loading) {
          this.loading$ = of(value.loading);
        }
      });
      this.store.dispatch(addPost({postsModel: this.postModel}));
      subscription.add(() => subscription.unsubscribe());
    });
  }


  onFileSelected(event: any) {
    const file = event.target.files[0];
    this.postModel.postImage = file
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        this.displayedImage = reader.result!
      };
      reader.readAsDataURL(file);
    }
    this.selectedImage = true;
  }
}
