import {Component, OnInit} from '@angular/core';
import {select, Store} from "@ngrx/store";
import {Observable, of} from "rxjs";
import {ConnectionRequestModel} from "../../../domain/model/connection-request.model";
import {
  acceptOrDenyConnectionRequest, addConnection,
  getPendingRequests,
  getSuggestions
} from "../../../store/connections/connections.actions";
import {
  selectAcceptOrDenyConnectionState, selectAddConnectionState,
  selectConnectionModel,
  selectGetSuggestionsState
} from "../../../store/connections/connections.selectors";
import {GetSuggestionsState} from "../../../store/connections/connections.reducer";

@Component({
  selector: 'app-connections',
  templateUrl: './connections.component.html',
  styleUrls: ['./connections.component.css']
})
export class ConnectionsComponent implements OnInit {
  pendingConnections: Observable<ConnectionRequestModel[]> | undefined;
  loading: boolean = false;
  accepted: boolean = false;
  state: Observable<GetSuggestionsState> = of()

  constructor(private store: Store) {
  }

  ngOnInit() {
    this.store.dispatch(getPendingRequests());
    this.pendingConnections = this.store.pipe(select(selectConnectionModel));
    this.store.dispatch(getSuggestions())
    this.state = this.store.select(selectGetSuggestionsState)
  }

  onAcceptConnectionRequest(userDecision: boolean, connectionRequestID: string) {
    this.store.dispatch(acceptOrDenyConnectionRequest({
        userDecision,
        connectionRequestID
      }
    ));
    this.store.pipe(select(selectAcceptOrDenyConnectionState)).subscribe((accepted) => {
      if (accepted.success && !this.accepted) {
        this.accepted = accepted.success;
        this.store.dispatch(getPendingRequests());
        this.pendingConnections = this.store.pipe(select(selectConnectionModel));
      }
    })
  }

  onAddConnection(destinationUserID:string){
    this.store.dispatch(addConnection({destinationUserID}));
    this.store.select(selectAddConnectionState).subscribe(
      value => {

      }
    )
  }


}
