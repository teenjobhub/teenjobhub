import {Component, OnInit} from '@angular/core';
import {select, Store} from "@ngrx/store";
import {firstValueFrom, Observable, of} from "rxjs";
import {ChatRoomModel} from "../../../domain/model/chat-room.model";
import {getChatRooms, getMessages, sendMessage} from "../../../store/chat/chat.actions";
import {LocalStorageService} from "../../../utils/localstorage";
import {
  selectGetChatRooms,
  selectGetMessages, selectSendMessagesState
} from "../../../store/chat/chat.selectors";
import {ChatMessageModel} from "../../../domain/model/chat-message.model";
import {IMqttMessage, MqttService} from "ngx-mqtt";

@Component({
  selector: 'app-chat-side-bar',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  public chatRooms: Observable<ChatRoomModel[]> = of([]);
  public messages: Observable<ChatMessageModel[]> = of([]);
  public selectedChatRoom: Observable<ChatRoomModel> = of()
  private myId = '';
  message: string ='';
  constructor(private store: Store, private localStorage: LocalStorageService,private _mqttService: MqttService,) {
    this._mqttService.observe(`/chat/${this.localStorage.getData('userProfile').id}`).subscribe((message: IMqttMessage) => {
      console.log(message.payload.toString())
      this.getChatRooms()
    });
  }

  ngOnInit(): void {
    this.myId = this.localStorage.getData('userProfile').id;
    this.getChatRooms()


  }

   getMessagesByChatRoom(chatRoomId:string){
    this.store.dispatch(getMessages({chatRoomId}))
    this.messages = this.store.select(selectGetMessages)
    this.messages.subscribe()
  }

  filterMembersByUserId(chatRooms: ChatRoomModel[], userId: string): ChatRoomModel[] {
    return chatRooms.map(chatRoom => ({
      ...chatRoom,
      members: chatRoom.members.filter(member => member.userProfile.id !== userId)
    }));
  }


  getChatRooms(){
    this.store.dispatch(getChatRooms(this.localStorage.getData('userProfile').id))
    this.chatRooms = this.store.pipe(select(selectGetChatRooms));
    this.chatRooms.subscribe(value => {
      if (value.length>0){
        let filteredValue = this.filterMembersByUserId(value, this.localStorage.getData('userProfile').id)
        this.chatRooms = of(filteredValue)
        this.selectedChatRoom = of(filteredValue[0])
        this.getMessagesByChatRoom(filteredValue[0].id)
      }
    })
  }

  onSelectUser(chatRoom: ChatRoomModel) {
    this.selectedChatRoom = of(chatRoom)
  }

  subscribeToMessagesChannel(){

  }

  itsMe(chatMessage: ChatMessageModel) {
    return chatMessage.sender.id == this.myId;
  }

 async sendMessage() {
   const chatRoom = await firstValueFrom(this.selectedChatRoom)
   this.store.dispatch(sendMessage({chatRoomId: chatRoom.id, message: this.message}))
   this.store.select(selectSendMessagesState).subscribe(
     async value => {
       if (value.success) {
         this.message=''
         this. getMessagesByChatRoom(chatRoom.id)
         this.messages.subscribe(
           value1 => {
             this.selectedChatRoom.subscribe(
               value2 => {
                 value2.last=value1.at(0)!
               }
             )
           }
         )
       }
     }
   ).unsubscribe()
 }
}
