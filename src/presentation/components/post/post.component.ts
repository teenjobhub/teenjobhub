import {Component, OnInit} from '@angular/core';
import {select, Store} from "@ngrx/store";
import {selectJobPostState} from "../../../store/post/post.selectors";
import {Observable, of} from "rxjs";
import {getPost} from "../../../store/post/post.actions";
import {DatePipe} from "@angular/common";
import {LocalStorageService} from "../../../utils/localstorage";
import {MatDialog} from "@angular/material/dialog";
import {ConfirmApplyComponent} from "../confirm-apply/confirm-apply.component";
import {JobPostState} from "../../../store/post/post.reducer";

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  posts: Observable<JobPostState> | undefined;
  loading: boolean = false;

  constructor(private store: Store, public datePipe: DatePipe, private localStorage: LocalStorageService, private dialog: MatDialog) {

  }

  ngOnInit() {
    this.store.dispatch(getPost());
    this.store.pipe(select(selectJobPostState)).subscribe((posts) => {

      this.posts = of(posts);

    })
  }

  react() {
  }

  comment() {

  }

  share() {

  }

  applyForJob(jobPostID: string, jobPostTitle: string) {
    this.dialog.open(ConfirmApplyComponent, {
      height: 'fit',
      width: '40%',
      data: {
        "jobPostID": jobPostID,
        "jobPostTitle": jobPostTitle,
      }, // Pass your parameter here
    });
  }

  showHireButton(id: string): boolean {
    return id == this.localStorage.getData('userProfile').id
  }
}
