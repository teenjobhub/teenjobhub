import {Component} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {PostDialogComponent} from "../post-dialog/post-dialog.component";
import {select, Store} from "@ngrx/store";
import {getPost} from "../../../store/post/post.actions";
import {selectJobPostState} from "../../../store/post/post.selectors";
import {LocalStorageService} from "../../../utils/localstorage";

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css']
})
export class AddPostComponent {
  constructor(public dialog: MatDialog, private store: Store, public localStorage: LocalStorageService) {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(PostDialogComponent, {
      height: 'fit',
      width: 'fit',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.store.dispatch(getPost());
      this.store.pipe(select(selectJobPostState)).subscribe(value => {
        if (value.jobPost) {

        } else if (value.loading) {
        }
      });
    });
  }
}
