import {Component, ElementRef, ViewChild} from '@angular/core';
import {select, Store} from "@ngrx/store";
import {Observable, of} from "rxjs";
import {NotificationsState} from "../../../store/notifications/notifications.reducer";
import {search} from "../../../store/user-profile/user-profile.actions";
import {selectSearchResults} from "../../../store/user-profile/user-profile.selectors";
import {UserProfileModel} from "../../../domain/model/user-profile.model";
import {Router} from "@angular/router";
import {UserImplementationRepository} from "../../../domain/repositories/user/user-implementation.repository";

@Component({
  selector: 'app-app-navbar',
  templateUrl: './app-navbar.component.html',
  styleUrls: ['./app-navbar.component.css']
})
export class AppNavbarComponent {
  state: Observable<NotificationsState> = of();

  isPopupVisible: boolean = false;
  suggestions: Observable<UserProfileModel[]> = of();
  popupTop: string = '0px';
  popupLeft: string = '0px';
  @ViewChild('searchField') searchField: ElementRef | undefined;

  constructor(private store: Store, private router: Router,private userRepository:UserImplementationRepository) {
  }


  showPopup(): void {
    this.isPopupVisible = true;
    this.setPosition();
  }

  hidePopup(): void {
    setTimeout(() => {
      this.isPopupVisible = false;
    }, 200);
  }

  onInput(event: Event): void {
    const target = event.target as HTMLInputElement;
    console.log('Search input:', target.value);
    this.store.dispatch(search({query: target.value}));
    this.store.pipe(select(selectSearchResults)).subscribe(
      value => {
        console.log(value)
        this.suggestions = of(value)
      }
    )
  }

  setPosition(): void {
    const rect = this.searchField?.nativeElement.getBoundingClientRect();
    this.popupTop = `${rect.bottom + window.scrollY}px`;
    this.popupLeft = `0`;
  }

  navigateToUserDetails(userProfileId: string) {
    this.router.navigate(['/main/user-details',], {queryParams: {userProfileId}}).then(value => {
    });
  }

  logout(){
    this.userRepository.logout().subscribe(value => {
      this.router.navigate(['/login',], ).then(value => {
      });
    })
  }

  protected readonly event = event;
}
