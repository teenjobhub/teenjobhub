import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from "../auth/login/login.component";
import {SignupComponent} from "../auth/signup/signup.component";
import {HomeComponent} from "../home/home.component";
import {ProfileSetupComponent} from "../profile-setup/profile-setup.component";
import {AuthGuard} from "../auth/auth-guard.guard";
import {MainAppComponent} from "../main-app/main-app.component";
import {ConnectionsComponent} from "../components/connections/connections.component";
import {NetworkComponent} from "../network/network.component";
import {JobSpaceComponent} from "../components/job-space/job-space.component";
import {JobsManagementComponent} from "../jobs-management/jobs-management.component";
import {NotificationsComponent} from "../notifications/notifications.component";
import {UserProfileRepositoryMapper} from "../../domain/repositories/user-profile/user-profile.repository.mapper";
import {UserDetailsComponent} from "../user-details/user-details.component";
import {ChatComponent} from "../components/chat/chat.component";

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'signup', component: SignupComponent},
  {
    path: 'main', component: MainAppComponent, canActivate: [AuthGuard], children: [
      {path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
      {path: 'my-jobs', component: JobsManagementComponent, canActivate: [AuthGuard]},
      {path: 'user-details', component: UserDetailsComponent, canActivate: [AuthGuard]},
      {path: 'chat', component: ChatComponent, canActivate: [AuthGuard]},
      {
        path: 'network', component: NetworkComponent, canActivate: [AuthGuard], children: [
          {path: 'connections', component: ConnectionsComponent,},
          {path: 'jobSpace', component: JobSpaceComponent,}
        ]
      },

    ]
  },
  {path: 'profile-setup', component: ProfileSetupComponent, canActivate: [AuthGuard]},
  {path: '', redirectTo: '/main', pathMatch: 'full'}, // Default route

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule {
}
