import {NgModule} from '@angular/core';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
import {AppComponent,} from './app.component';
import {LoginComponent} from '../auth/login/login.component';
import {SignupComponent} from '../auth/signup/signup.component';
import {RouterLink, RouterOutlet} from "@angular/router";
import {AppRoutingModule} from './app-routing.module';
import {CommonModule, DatePipe, NgOptimizedImage} from "@angular/common";
import {ProfileSetupComponent} from "../profile-setup/profile-setup.component";
import {HomeComponent} from "../home/home.component";
import {FormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'; // CLI imports AppRoutingModule
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatIconModule, MatIconRegistry} from "@angular/material/icon";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatCardModule} from "@angular/material/card";
import {MatListModule} from "@angular/material/list";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {skillReducer} from "../../store/skill/skill.reducer";
import {SkillEffects} from "../../store/skill/skill.effects";
import {occupationReducer} from "../../store/occupation/occupation.reducer";
import {OccupationEffects} from "../../store/occupation/occupation.effects";
import {MatSelectCountryModule} from "@angular-material-extensions/select-country";
import {getProfileByIdReducer, searchReducer, userProfileReducer} from "../../store/user-profile/user-profile.reducer";
import {UserProfileEffects} from "../../store/user-profile/user-profile.effects";
import {AppNavbarComponent} from "../components/app-navbar/app-navbar.component";
import {DataModule} from "../../domain/data/data.module";
import {PostComponent} from "../components/post/post.component";
import {MatDialogModule} from "@angular/material/dialog";
import {PostDialogComponent} from "../components/post-dialog/post-dialog.component";
import {AddPostComponent} from "../components/add-post/add-post.component";
import {UserProfileSideBarComponent} from "../components/user-profile-side-bar/user-profile-side-bar.component";
import {loginReducer} from "../../store/auth/login/login.reducer";
import {LoginEffects} from "../../store/auth/login/login.effects";
import {AuthInterceptor} from "../../utils/interceptor";
import {
  getJobsByUserReducer,
  hiredReducer,
  jobPostReducer,
  myApplicationsReducer,
  postReducer, rateReducer
} from "../../store/post/post.reducer";
import {PostEffects} from "../../store/post/post.effects";
import {signupReducer} from "../../store/auth/signup/signup.reducer";
import {SignupEffects} from "../../store/auth/signup/signup.effects";
import {MainAppComponent} from "../main-app/main-app.component";
import {MatMenuModule} from "@angular/material/menu";
import {ConnectionsComponent} from "../components/connections/connections.component";
import {
  addConnectionStateReducer,
  connectionsReducer,
  getSuggestionsStateReducer,
  getUserConnectionsStateReducer
} from "../../store/connections/connections.reducer";
import {ConnectionsEffects} from "../../store/connections/connections.effects";
import {NetworkComponent} from "../network/network.component";
import {JobSpaceComponent} from "../components/job-space/job-space.component";
import {getUserJobSpacesStateReducer, jobspaceReducer} from "../../store/jobspace/jobspace.reducer";
import {JobspaceEffects} from "../../store/jobspace/jobspace.effects";
import {JobSpaceDialogComponent} from "../components/job-space-dialog/job-space-dialog.component";
import {IMqttServiceOptions, MqttModule} from 'ngx-mqtt';
import {MatBadgeModule} from "@angular/material/badge";
import {ChatComponent} from "../components/chat/chat.component";
import {JobsManagementComponent} from "../jobs-management/jobs-management.component";
import {NotificationsComponent} from "../notifications/notifications.component";
import {notificationsReducer} from "../../store/notifications/notifications.reducer";
import {NotificationsEffects} from "../../store/notifications/notifications.effects";
import {HireDialogComponent} from '../components/hire-dialog/hire-dialog.component';
import {UserDetailsComponent} from '../user-details/user-details.component';
import {chatRoomReducer, chatRoomsReducer, messagesReducer, sendMessagesReducer} from "../../store/chat/chat.reducer";
import {ChatEffects} from "../../store/chat/chat.effects";
import { ConfirmApplyComponent } from '../components/confirm-apply/confirm-apply.component';
import { RatingComponent } from '../components/rating/rating.component';


export const MQTT_SERVICE_OPTIONS: IMqttServiceOptions = {
  hostname: '167.86.106.30',
  port: 8083,
  path: '/mqtt'
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    ProfileSetupComponent,
    HomeComponent,
    AppNavbarComponent,
    PostComponent,
    PostDialogComponent,
    AddPostComponent,
    UserProfileSideBarComponent,
    MainAppComponent,
    ConnectionsComponent,
    NetworkComponent,
    JobSpaceComponent,
    JobSpaceDialogComponent,
    ChatComponent,
    JobsManagementComponent,
    NotificationsComponent,
    HireDialogComponent,
    UserDetailsComponent,
    ChatComponent,
    ConfirmApplyComponent,
    RatingComponent,
  ],
  imports: [
    BrowserModule,
    RouterOutlet,
    RouterLink,
    AppRoutingModule,
    NgOptimizedImage,
    FormsModule,
    CommonModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatDialogModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatInputModule,
    MatButtonModule,
    FlexLayoutModule,
    MatCardModule,
    MatListModule,
    MatCheckboxModule,
    MatBadgeModule,
    StoreModule.forRoot({
      skills: skillReducer,
      Occupations: occupationReducer,
      userProfile: userProfileReducer,
      tokenModel: loginReducer,
      post: postReducer,
      posts: getJobsByUserReducer,
      jobPost: jobPostReducer,
      userModel: signupReducer,
      connectionModel: connectionsReducer,
      jobSpaceModel: jobspaceReducer,
      jobSpaces: getUserJobSpacesStateReducer,
      userProfileList: getUserConnectionsStateReducer,
      Notifications: notificationsReducer,
      hired: hiredReducer,
      added: addConnectionStateReducer,
      profiles: getSuggestionsStateReducer,
      userProfiles: searchReducer,
      profile: getProfileByIdReducer,
      chatRoom: chatRoomReducer,
      chatRooms: chatRoomsReducer,
      messages: messagesReducer,
      sent: sendMessagesReducer,
      myApplications:myApplicationsReducer,
      rate:rateReducer
    }, {}),
    EffectsModule.forRoot([
      SkillEffects,
      OccupationEffects,
      UserProfileEffects,
      LoginEffects,
      PostEffects,
      SignupEffects,
      ConnectionsEffects,
      JobspaceEffects,
      NotificationsEffects,
      ChatEffects
    ]),
    MatSelectCountryModule.forRoot('de'),
    DataModule,
    MatMenuModule,
    MqttModule.forRoot(MQTT_SERVICE_OPTIONS),


  ],
  providers: [
    DatePipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer) {
    const icons: { [key: string]: string } = {
      'home': 'assets/icons/home.svg',
      'user': 'assets/icons/user.svg',
      'notifications': 'assets/icons/bell.svg',
      'arrow-down': 'assets/icons/down-arrow.svg',
      'people': 'assets/icons/social-network.svg',
      'connections': 'assets/icons/group.svg',
      'job-space': 'assets/icons/work-from-home.svg',

    };
    Object.keys(icons).forEach(iconName => {
      this.matIconRegistry.addSvgIcon(
        iconName,
        this.domSanitizer.bypassSecurityTrustResourceUrl(icons[iconName])
      );
    });
  }
}
