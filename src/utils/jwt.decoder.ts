import {Injectable} from '@angular/core';
import {jwtDecode} from 'jwt-decode';
import {LocalStorageService} from "./localstorage";
import {TokenModel} from "../domain/model/token.model";

@Injectable({
  providedIn: 'root'
})
export class JwtDecodeService {
  constructor(private localStorage: LocalStorageService,) {
  }

  decodeToken(token: TokenModel): any {
    try {
      return jwtDecode(token.access_token);
    } catch (Error) {
      return null;
    }
  }

  getUserIDFromToken(): string {
    return this.decodeToken(this.localStorage.getData('token')).userID;
  }
}
