import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest,} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Router} from '@angular/router';
import {LocalStorageService} from "./localstorage";
import {JwtDecodeService} from "./jwt.decoder";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private localStorage: LocalStorageService,
    private router: Router,
    private jwtDecoder: JwtDecodeService
  ) {
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const token = this.localStorage.getData('token');
    if (request.url.includes('token') || request.url.includes('signup')) {
      return next.handle(request);
    } else if (!token || this.jwtDecoder.decodeToken(token).exp < Date.now() / 1000) {
      this.router.navigate(['/login']);
      return throwError('No valid access token');
    } else {
      const authReq = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token.access_token}`,
        },
      });
      return next.handle(authReq).pipe(
        catchError((error) => {
          return throwError(error);
        })
      );
    }
  }
}
