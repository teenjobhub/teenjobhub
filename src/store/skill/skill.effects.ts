import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import * as SkillActions from 'src/store/skill/skills.actions';
import {GetSkillsUsecase} from "../../domain/usecases/skills/get-skills.usecase";

@Injectable()
export class SkillEffects {
  loadSkills$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SkillActions.loadSkills),
      switchMap(() =>
        this.skillsUsecase.execute().pipe(
          map((skills) => SkillActions.loadSkillsSuccess({skills})),
          catchError((error) => of(SkillActions.loadSkillsFailure({error})))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private skillsUsecase: GetSkillsUsecase
  ) {
  }
}
