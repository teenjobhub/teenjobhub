import {createReducer, on} from '@ngrx/store';
import * as SkillActions from 'src/store/skill/skills.actions';
import {SkillModel} from "../../domain/model/skill.model";

export interface SkillState {
  skills: SkillModel[];
  loading: boolean;
  error: any;
}

export const initialState: SkillState = {
  skills: [],
  loading: false,
  error: null,
};

export const skillReducer = createReducer(
  initialState,
  on(SkillActions.loadSkills, (state) => ({...state, loading: true})),
  on(SkillActions.loadSkillsSuccess, (state, {skills}) => ({...state, skills, loading: false, error: null})),
  on(SkillActions.loadSkillsFailure, (state, {error}) => ({...state, loading: false, error}))
);
