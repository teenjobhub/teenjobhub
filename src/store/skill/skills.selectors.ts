import {createFeatureSelector, createSelector} from '@ngrx/store';
import {SkillState} from 'src/store/skill/skill.reducer';

export const selectSkillState = createFeatureSelector<SkillState>('skills');

export const selectSkills = createSelector(
  selectSkillState,
  (state: SkillState) => state.skills
);
