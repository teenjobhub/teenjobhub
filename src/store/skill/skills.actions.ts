import {createAction, props} from '@ngrx/store';
import {SkillModel} from 'src/domain/model/skill.model';

export const loadSkills = createAction('[Skills] Load Skills');
export const loadSkillsSuccess = createAction('[Skills] Load Skills Success', props<{ skills: SkillModel[] }>());
export const loadSkillsFailure = createAction('[Skills] Load Skills Failure', props<{ error: any }>());
