import {createReducer, on} from '@ngrx/store';
import * as NotificationsActions from "src/store/notifications/notifications.actions"
import {NotificationModel} from "../../domain/model/notifications.model";

export interface NotificationsState {
  Notifications: NotificationModel[];
  loading: boolean;
  error: any;
}

export const initialState: NotificationsState = {
  Notifications: [],
  loading: false,
  error: null,
};

export const notificationsReducer = createReducer(
  initialState,
  on(NotificationsActions.loadNotifications, (state) => ({...state, loading: true})),
  on(NotificationsActions.loadNotificationsSuccess, (state, {Notifications}) => ({
    ...state,
    Notifications,
    loading: false,
    error: null
  })),
  on(NotificationsActions.loadNotificationsFailure, (state, {error}) => ({...state, loading: false, error}))
);
