import {createAction, props} from '@ngrx/store';
import {NotificationModel} from "../../domain/model/notifications.model";

export const loadNotifications = createAction('[Notifications] Load Notifications');
export const loadNotificationsSuccess = createAction('[Notifications] Load Notifications Success', props<{
  Notifications: NotificationModel[]
}>());
export const loadNotificationsFailure = createAction('[Notifications] Load Notifications Failure', props<{
  error: any
}>());
