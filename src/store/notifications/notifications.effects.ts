import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import * as NotificationsActions from 'src/store/notifications/notifications.actions';
import {GetOccupationUsecase} from "../../domain/usecases/occupation/get-occupation.usecase";
import {GetNotificationUsecase} from "../../domain/usecases/notification/get-notification.usecase";

@Injectable()
export class NotificationsEffects {
  loadNotificationsOccupations$ = createEffect(() =>
    this.actions$.pipe(
      ofType(NotificationsActions.loadNotifications),
      switchMap(() =>
        this.getNotificationUsecase.execute().pipe(
          map((Notifications) => NotificationsActions.loadNotificationsSuccess({Notifications})),
          catchError((error) => of(NotificationsActions.loadNotificationsFailure({error})))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private getNotificationUsecase: GetNotificationUsecase
  ) {
  }
}
