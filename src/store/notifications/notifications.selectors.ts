import {createFeatureSelector, createSelector} from '@ngrx/store';
import {NotificationsState} from "./notifications.reducer";

export const selectNotificationsState = createFeatureSelector<NotificationsState>('Notifications');

export const selectNotifications = createSelector(
  selectNotificationsState,
  (state: NotificationsState) => state.Notifications
);
