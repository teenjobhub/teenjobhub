import {createFeatureSelector, createSelector} from '@ngrx/store';
import {
  AcceptOrDenyConnectionState,
  AddConnectionState,
  ConnectionState, GetSuggestionsState,
  GetUserConnectionsState
} from './connections.reducer';

export const selectConnectionState = createFeatureSelector<ConnectionState>('connectionModel');

export const selectConnectionModel = createSelector(
  selectConnectionState,
  (state: ConnectionState) => state.connectionModel
);

export const selectAcceptOrDenyConnectionState = createFeatureSelector<AcceptOrDenyConnectionState>('connectionModel');

export const selectAcceptedModel = createSelector(
  selectAcceptOrDenyConnectionState,
  (state: AcceptOrDenyConnectionState) => state.accepted
);

export const selectGetUserConnectionsState = createFeatureSelector<GetUserConnectionsState>('userProfileList');

export const selectUserConnections = createSelector(
  selectGetUserConnectionsState,
  (state: GetUserConnectionsState) => state.userProfileList
);

export const selectAddConnectionState = createFeatureSelector<AddConnectionState>('addConnectionModel');

export const selectConnection = createSelector(
  selectAddConnectionState,
  (state: AddConnectionState) => state.added
);

export const selectGetSuggestionsState = createFeatureSelector<GetSuggestionsState>('profiles');

export const selectSuggestions = createSelector(
    selectGetSuggestionsState,
    (state: GetSuggestionsState) => state.profiles
);
