import {createAction, props} from '@ngrx/store';
import {ConnectionRequestModel} from "../../domain/model/connection-request.model";
import {UserProfileModel} from "../../domain/model/user-profile.model";

export const getPendingRequests = createAction('[Connection] Connection',);
export const getPendingRequestsSuccess = createAction('[Connection] Connection Success', props<{
  connectionModel: ConnectionRequestModel[]
}>());
export const getPendingRequestsFailure = createAction('[Connection] Connection  Failure', props<{ error: any }>());

export const acceptOrDenyConnectionRequest = createAction('[Connection] AcceptOrDenyConnection', props<{
  userDecision: boolean,
  connectionRequestID: string
}>());
export const acceptOrDenyConnectionRequestSuccess = createAction('[Connection] AcceptOrDenyConnection Success', props<{
  accepted: boolean
}>());
export const acceptOrDenyConnectionRequestFailure = createAction('[Connection] AcceptOrDenyConnection  Failure', props<{
  error: any
}>());

export const getUserConnections = createAction('[Connection] GetUserConnection',);
export const getUserConnectionsSuccess = createAction('[Connection] GetUserConnection Success', props<{
  userProfileList: ConnectionRequestModel[]
}>());
export const getUserConnectionsFailure = createAction('[Connection] GetUserConnection  Failure', props<{
  error: any
}>());

export const addConnection = createAction('[Added] AddConnection', props<{
  destinationUserID: string
}>());
export const addConnectionSuccess = createAction('[Added] AddConnection Success', props<{
  added: boolean
}>());
export const addConnectionFailure = createAction('[Added] AddConnection  Failure', props<{ error: any }>());

export const getSuggestions = createAction('[Profiles] Suggestions');
export const getSuggestionsSuccess = createAction('[Profiles] Suggestions Success', props<{
  profiles: UserProfileModel[]
}>());
export const getSuggestionsFailure = createAction('[Profiles] Suggestions Failure', props<{ error: any }>());
