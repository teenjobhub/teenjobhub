import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import * as ConnectionAction from 'src/store/connections/connections.actions';
import {GetPendingConnectionsUsecase} from "../../domain/usecases/connection/get-pending-connections.usecase";
import {AcceptordenyConnectionUsecase} from "../../domain/usecases/connection/acceptordeny-connection.usecase";
import {GetUserConnectionsUsecase} from "../../domain/usecases/connection/get-user-connections.usecase";
import {AddConnectionUsecase} from "../../domain/usecases/connection/add-connection.usecase";
import {GetSuggestionsUsecase} from "../../domain/usecases/connection/get-suggestions.usecase";

@Injectable()
export class ConnectionsEffects {
    getPendingConnections$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ConnectionAction.getPendingRequests),
            switchMap((action) =>
                this.getPendingConnections.execute().pipe(
                    map((connectionModel) => ConnectionAction.getPendingRequestsSuccess({connectionModel})),
                    catchError((error) => of(ConnectionAction.getPendingRequestsFailure({error})))
                )
            )
        )
    );

    acceptOrDenyConnectionRequest$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ConnectionAction.acceptOrDenyConnectionRequest),
            switchMap((action) =>
                this.acceptordenyConnectionUsecase.execute(action).pipe(
                    map((accepted) => ConnectionAction.acceptOrDenyConnectionRequestSuccess({accepted: accepted})),
                    catchError((error) => of(ConnectionAction.getPendingRequestsFailure({error})))
                )
            )
        )
    );

    getUserConnections$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ConnectionAction.getUserConnections),
            switchMap((action) =>
                this.getUserConnectionsUsecase.execute().pipe(
                    map((userProfileList) => ConnectionAction.getUserConnectionsSuccess({userProfileList: userProfileList})),
                    catchError((error) => of(ConnectionAction.getUserConnectionsFailure({error})))
                )
            )
        )
    );

    addConnections$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ConnectionAction.addConnection),
            switchMap((action) =>
                this.addConnectionUsecase.execute(action).pipe(
                    map((added) => ConnectionAction.addConnectionSuccess({added: added})),
                    catchError((error) => of(ConnectionAction.addConnectionFailure({error})))
                )
            )
        )
    );

    getSuggestions = createEffect(() =>
        this.actions$.pipe(
            ofType(ConnectionAction.getSuggestions),
            switchMap((action) =>
                this.getSuggestionsUsecase.execute().pipe(
                    map((profiles) => ConnectionAction.getSuggestionsSuccess({profiles: profiles})),
                    catchError((error) => of(ConnectionAction.getSuggestionsFailure({error})))
                )
            )
        )
    );

    constructor(
        private actions$: Actions,
        private getPendingConnections: GetPendingConnectionsUsecase,
        private acceptordenyConnectionUsecase: AcceptordenyConnectionUsecase,
        private getUserConnectionsUsecase: GetUserConnectionsUsecase,
        private addConnectionUsecase: AddConnectionUsecase,
        private getSuggestionsUsecase: GetSuggestionsUsecase,
    ) {
    }
}
