import {createReducer, on} from '@ngrx/store';
import * as ConnectionAction from 'src/store/connections/connections.actions';
import {ConnectionRequestModel} from "../../domain/model/connection-request.model";
import {UserProfileModel} from "../../domain/model/user-profile.model";

export interface ConnectionState {
  connectionModel: ConnectionRequestModel[];
  loading: boolean;
  success: boolean;
  error: any;
}

export const initialState: ConnectionState = {
  connectionModel: [],
  loading: false,
  success: false,
  error: null,
};

export const connectionsReducer = createReducer(
  initialState,
  on(ConnectionAction.getPendingRequests, (state) => ({...state, loading: true})),
  on(ConnectionAction.getPendingRequestsSuccess, (state, {connectionModel}) => ({
    ...state,
    connectionModel,
    loading: false,
    error: null,
    success: true
  })),
  on(ConnectionAction.getPendingRequestsFailure, (state, {error}) => ({...state, loading: false, error}))
);

export interface AcceptOrDenyConnectionState {
  accepted: boolean;
  loading: boolean;
  success: boolean;
  error: any;
}

export const AcceptOrDenyConnectionInitialState: AcceptOrDenyConnectionState = {
  accepted: false,
  loading: false,
  success: false,
  error: null,
};

export const acceptOrDenyConnectionReducer = createReducer(
  AcceptOrDenyConnectionInitialState,
  on(ConnectionAction.acceptOrDenyConnectionRequest, (state) => ({...state, loading: true})),
  on(ConnectionAction.acceptOrDenyConnectionRequestSuccess, (state, {accepted}) => ({
    ...state,
    accepted,
    loading: false,
    error: null,
    success: true
  })),
  on(ConnectionAction.acceptOrDenyConnectionRequestFailure, (state, {error}) => ({...state, loading: false, error}))
);

export interface GetUserConnectionsState {
  userProfileList: ConnectionRequestModel[];
  loading: boolean;
  success: boolean;
  error: any;
}

export const getUserConnectionsInitialState: GetUserConnectionsState = {
  userProfileList: [],
  loading: false,
  success: false,
  error: null,
};

export const getUserConnectionsStateReducer = createReducer(
  getUserConnectionsInitialState,
  on(ConnectionAction.getUserConnections, (state) => ({...state, loading: true})),
  on(ConnectionAction.getUserConnectionsSuccess, (state, {userProfileList}) => ({
    ...state,
    userProfileList: userProfileList,
    loading: false,
    error: null,
    success: true
  })),
  on(ConnectionAction.getUserConnectionsFailure, (state, {error}) => ({...state, loading: false, error}))
);

export interface AddConnectionState {
  added: boolean;
  loading: boolean;
  success: boolean;
  error: any;
}

export const addConnectionInitialState: AddConnectionState = {
  added:false,
  loading: false,
  success: false,
  error: null,
};

export const addConnectionStateReducer = createReducer(
  addConnectionInitialState,
  on(ConnectionAction.addConnection, (state) => ({...state, loading: true})),
  on(ConnectionAction.addConnectionSuccess, (state, {added}) => ({

    ...state,
    added,
    loading: false,
    error: null,
    success: true
  })),
  on(ConnectionAction.addConnectionFailure, (state, {error}) => ({...state, loading: false, error}))
);
export interface GetSuggestionsState {
    profiles: UserProfileModel[];
    loading: boolean;
    success: boolean;
    error: any;
}

export const getSuggestionsInitialState: GetSuggestionsState = {
    profiles:[],
    loading: false,
    success: false,
    error: null,
};

export const getSuggestionsStateReducer = createReducer(
    getSuggestionsInitialState,
    on(ConnectionAction.getSuggestions, (state) => ({...state, loading: true})),
    on(ConnectionAction.getSuggestionsSuccess, (state, {profiles}) => ({
        ...state,
        profiles,
        loading: false,
        error: null,
        success: true
    })),
    on(ConnectionAction.getUserConnectionsFailure, (state, {error}) => ({...state, loading: false, error}))
);
