import {createAction, props} from '@ngrx/store';
import {TokenModel} from "../../../domain/model/token.model";

export const login = createAction('[Login] Login', props<{ username: string, password: string }>());
export const loginSuccess = createAction('[Login] Login Success', props<{ tokenModel: TokenModel }>());
export const loginFailure = createAction('[Login] Login  Failure', props<{ error: any }>());
