import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import * as LoginAction from 'src/store/auth/login/login.actions';
import {UserLoginUseCase} from "../../../domain/usecases/user/user-login.usecase";

@Injectable()
export class LoginEffects {
  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LoginAction.login),
      switchMap((action) =>
        this.getUserUseCase.execute(action).pipe(
          map((tokenModel) => LoginAction.loginSuccess({tokenModel})),
          catchError((error) => of(LoginAction.loginFailure({error})))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private getUserUseCase: UserLoginUseCase
  ) {
  }
}
