import {createFeatureSelector, createSelector} from '@ngrx/store';
import {LoginState} from "./login.reducer";


export const selectLoginState = createFeatureSelector<LoginState>('tokenModel');

export const selectUser = createSelector(
  selectLoginState,
  (state: LoginState) => state.tokenModel
);
