import {createReducer, on} from '@ngrx/store';
import * as LoginActions from 'src/store/auth/login/login.actions';
import {TokenModel} from "../../../domain/model/token.model";

export interface LoginState {
  tokenModel: TokenModel;
  loading: boolean;
  success: boolean;
  error: any;
}

export const initialState: LoginState = {
  tokenModel: {
    access_token: '',
    refresh_token: '',
  },
  loading: false,
  success: false,
  error: null,
};

export const loginReducer = createReducer(
  initialState,
  on(LoginActions.login, (state) => ({...state, loading: true})),
  on(LoginActions.loginSuccess, (state, {tokenModel}) => ({
    ...state,
    tokenModel,
    loading: false,
    error: null,
    success: true
  })),
  on(LoginActions.loginFailure, (state, {error}) => ({...state, loading: false, error}))
);
