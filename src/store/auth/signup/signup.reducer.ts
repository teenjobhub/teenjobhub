import {createReducer, on} from '@ngrx/store';
import * as SignUpAction from 'src/store/auth/signup/signup.actions';
import {UserModel} from "../../../domain/model/user.model";

export interface SignUpState {
  userModel: UserModel;
  loading: boolean;
  success: boolean;
  error: any;
}

export const initialState: SignUpState = {
  userModel: {
    id: '',
    username: '',
    email: '',
    token: ''
  },
  loading: false,
  success: false,
  error: null,
};

export const signupReducer = createReducer(
  initialState,
  on(SignUpAction.signUp, (state) => ({...state, loading: true})),
  on(SignUpAction.signUpSuccess, (state, {userModel}) => ({
    ...state,
    userModel: userModel,
    loading: false,
    error: null,
    success: true
  })),
  on(SignUpAction.signUpFailure, (state, {error}) => ({...state, loading: false, error}))
);
