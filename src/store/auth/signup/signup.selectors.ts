import {createFeatureSelector, createSelector} from '@ngrx/store';
import {SignUpState} from "./signup.reducer";


export const selectSignUpState = createFeatureSelector<SignUpState>('userModel');

export const selectUser = createSelector(
  selectSignUpState,
  (state: SignUpState) => state.userModel
);
