import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import {UserRegisterUseCase} from "../../../domain/usecases/user/user-register.usecase";
import * as SignUpAction from 'src/store/auth/signup/signup.actions';

@Injectable()
export class SignupEffects {
  signUp = createEffect(() => {
      return this.actions$.pipe(
        ofType(SignUpAction.signUp),
        switchMap((action) =>
          this.userRegisterUseCase.execute(action).pipe(
            map((userModel) => SignUpAction.signUpSuccess({userModel})),
            catchError((error) => of(SignUpAction.signUpFailure({error})))
          )
        )
      )

    }
  );

  constructor(
    private actions$: Actions,
    private userRegisterUseCase: UserRegisterUseCase
  ) {
  }


}
