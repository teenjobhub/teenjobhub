import {createAction, props} from '@ngrx/store';
import {UserModel} from "../../../domain/model/user.model";


export const signUp = createAction('[UserModel] signUp', props<{
  email: string,
  password: string,
  username: string,
}>());
export const signUpSuccess = createAction('[UserModel] signUp Success', props<{ userModel: UserModel }>());
export const signUpFailure = createAction('[UserModel] signUp Failure', props<{ error: any }>());
