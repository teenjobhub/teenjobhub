import {createAction, props} from '@ngrx/store';
import {JobSpaceModel} from "../../domain/model/job-space.model";
import {UserProfileModel} from "../../domain/model/user-profile.model";

export const getJobSpacesPendingRequests = createAction('[JobSpace] JobSpace',);
export const getJobSpacePendingRequestsSuccess = createAction('[JobSpace] JobSpace Success', props<{
  jobSpaceModel: JobSpaceModel[]
}>());
export const getJobSpacePendingRequestsFailure = createAction('[JobSpace] JobSpace  Failure', props<{ error: any }>());

export const createJobSpaceRequests = createAction('[JobSpace] CreateJobSpaceRequests', props<{
  jobSpaceModel: JobSpaceModel
}>());
export const createJobSpaceSuccess = createAction('[JobSpace] CreateJobSpaceRequests Success', props<{
  jobSpaceModel: JobSpaceModel
}>());
export const createJobSpaceRequestsFailure = createAction('[JobSpace] CreateJobSpaceRequests  Failure', props<{
  error: any
}>());

export const addUserToJobSpaceRequests = createAction('[JobSpace] AddUserToJobSpace', props<{
  jobSpaceID: string, userProfile: UserProfileModel
}>());
export const addUserToJobSpaceSuccess = createAction('[JobSpace] AddUserToJobSpace Success', props<{
  jobSpaceModel: JobSpaceModel
}>());
export const addUserToJobSpaceRequestsFailure = createAction('[JobSpace] AddUserToJobSpace Failure', props<{
  error: any
}>());

export const acceptOrDenyInvitationRequests = createAction('[JobSpace] AcceptOrDenyInvitationJobSpaceRequests', props<{
  jobSpaceID: string,
  userProfileID: string,
  userDecision: boolean
}>());
export const acceptOrDenyInvitationSuccess = createAction('[JobSpace] AcceptOrDenyInvitationJobSpaceRequests Success', props<{
  accepted: boolean
}>());
export const acceptOrDenyInvitationFailure = createAction('[JobSpace] AcceptOrDenyInvitationJobSpaceRequests Failure', props<{
  error: any
}>());

export const getUserJobSpacesRequests = createAction('[JobSpace] GetUserJobSpacesRequests',);
export const getUserJobSpacesSuccess = createAction('[JobSpace] GetUserJobSpacesRequests Success', props<{
  jobSpaces: JobSpaceModel[]
}>());
export const getUserJobSpacesFailure = createAction('[JobSpace] GetUserJobSpacesRequests Failure', props<{
  error: any
}>());
