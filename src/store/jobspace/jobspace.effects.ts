import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import * as JobSpaceAction from 'src/store/jobspace/jobspace.actions';
import {GetPendingInvitationsUsecase} from "../../domain/usecases/jobspace/get-pending-invitations.usecase";
import {CreateJobspaceUsecase} from "../../domain/usecases/jobspace/create-jobspace-usecase";
import {AddUserToJobspaceUsecase} from "../../domain/usecases/jobspace/add-user-to-jobspace.usecase";
import {AcceptOrDenyJobSpaceInvitationUsecase} from "../../domain/usecases/jobspace/accept-or-deny-invitation.usecase";
import {GetUserJobspaces} from "../../domain/usecases/jobspace/get-user-jobspaces";

@Injectable()
export class JobspaceEffects {
  getPendingJobSpacesInvitations$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JobSpaceAction.getJobSpacesPendingRequests),
      switchMap((action) =>
        this.getPendingInvitationsUsecase.execute().pipe(
          map((jobSpaceModel) => JobSpaceAction.getJobSpacePendingRequestsSuccess({jobSpaceModel})),
          catchError((error) => of(JobSpaceAction.getJobSpacePendingRequestsFailure({error})))
        )
      )
    )
  );

  createJobSpace$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JobSpaceAction.createJobSpaceRequests),
      switchMap((action) =>
        this.createJobspaceUsecase.execute({jobSpace: action.jobSpaceModel}).pipe(
          map((jobSpaceModel) => JobSpaceAction.createJobSpaceSuccess({jobSpaceModel})),
          catchError((error) => of(JobSpaceAction.createJobSpaceRequestsFailure({error})))
        )
      )
    )
  );

  addUserToJobSpace$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JobSpaceAction.addUserToJobSpaceRequests),
      switchMap((action) =>
        this.addUserToJobspaceUsecase.execute(action).pipe(
          map((jobSpaceModel) => JobSpaceAction.createJobSpaceSuccess({jobSpaceModel})),
          catchError((error) => of(JobSpaceAction.addUserToJobSpaceRequestsFailure({error})))
        )
      )
    )
  );

  acceptOrDenyInvitation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JobSpaceAction.acceptOrDenyInvitationRequests),
      switchMap((action) =>
        this.acceptOrDenyJobSpaceInvitationUsecase.execute(action).pipe(
          map((accepted) => JobSpaceAction.acceptOrDenyInvitationSuccess({accepted: accepted,})),
          catchError((error) => of(JobSpaceAction.addUserToJobSpaceRequestsFailure({error})))
        )
      )
    )
  );

  getUserJobSpaces$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JobSpaceAction.getUserJobSpacesRequests),
      switchMap((action) =>
        this.getUserJobspaces.execute().pipe(
          map((jobSpaces) => JobSpaceAction.getUserJobSpacesSuccess({jobSpaces})),
          catchError((error) => of(JobSpaceAction.getUserJobSpacesFailure({error})))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private getPendingInvitationsUsecase: GetPendingInvitationsUsecase,
    private createJobspaceUsecase: CreateJobspaceUsecase,
    private addUserToJobspaceUsecase: AddUserToJobspaceUsecase,
    private acceptOrDenyJobSpaceInvitationUsecase: AcceptOrDenyJobSpaceInvitationUsecase,
    private getUserJobspaces: GetUserJobspaces,
  ) {
  }
}
