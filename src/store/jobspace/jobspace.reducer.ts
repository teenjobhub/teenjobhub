import {createReducer, on} from '@ngrx/store';
import * as JobSpaceAction from 'src/store/jobspace/jobspace.actions';
import {JobSpaceModel} from "../../domain/model/job-space.model";

export interface JobSpaceState {
  jobSpaceModel: JobSpaceModel[];
  loading: boolean;
  success: boolean;
  error: any;
}

export const initialState: JobSpaceState = {
  jobSpaceModel: [],
  loading: false,
  success: false,
  error: null,
};

export const jobspaceReducer = createReducer(
  initialState,
  on(JobSpaceAction.getJobSpacesPendingRequests, (state) => ({...state, loading: true})),
  on(JobSpaceAction.getJobSpacePendingRequestsSuccess, (state, {jobSpaceModel}) => ({
    ...state,
    jobSpaceModel,
    loading: false,
    error: null,
    success: true
  })),
  on(JobSpaceAction.getJobSpacePendingRequestsFailure, (state, {error}) => ({...state, loading: false, error}))
);

export interface CreateJobSpaceState {
  jobSpaceModel: JobSpaceModel;
  loading: boolean;
  success: boolean;
  error: any;
}

export const createJobSpaceStateInitialState: CreateJobSpaceState = {
  jobSpaceModel: {
    name: '',
    admin: {
      id: '',
      firstName: '',
      lastName: '',
      phoneNumber: '',
      country: '',
      governance: '',
      userImage: '',
      user: {
        id: '',
        username: '',
        email: '',
        token: ''
      },
      occupation: {
        id: '',
        name: ''
      },
      skills: []
    },
    staff: [],
    id: ''
  },
  loading: false,
  success: false,
  error: null,
};

export const CreateJobSpaceStateReducer = createReducer(
  createJobSpaceStateInitialState,
  on(JobSpaceAction.createJobSpaceRequests, (state) => ({...state, loading: true})),
  on(JobSpaceAction.createJobSpaceSuccess, (state, {jobSpaceModel}) => ({
    ...state,
    jobSpaceModel,
    loading: false,
    error: null,
    success: true
  })),
  on(JobSpaceAction.createJobSpaceRequestsFailure, (state, {error}) => ({...state, loading: false, error}))
);

export interface AddUserToJobSpaceState {
  jobSpaceModel: JobSpaceModel;
  loading: boolean;
  success: boolean;
  error: any;
}

export const addUserToJobSpaceInitialState: AddUserToJobSpaceState = {
  jobSpaceModel: {
    name: '',
    admin: {
      id: '',
      firstName: '',
      lastName: '',
      phoneNumber: '',
      country: '',
      governance: '',
      userImage: '',
      user: {
        id: '',
        username: '',
        email: '',
        token: ''
      },
      occupation: {
        id: '',
        name: ''
      },
      skills: []
    },
    staff: [],
    id: ''
  },
  loading: false,
  success: false,
  error: null,
};

export const AddUserToJobSpaceStateReducer = createReducer(
  addUserToJobSpaceInitialState,
  on(JobSpaceAction.addUserToJobSpaceRequests, (state) => ({...state, loading: true})),
  on(JobSpaceAction.addUserToJobSpaceSuccess, (state, {jobSpaceModel}) => ({
    ...state,
    jobSpaceModel,
    loading: false,
    error: null,
    success: true
  })),
  on(JobSpaceAction.addUserToJobSpaceRequestsFailure, (state, {error}) => ({...state, loading: false, error}))
);

export interface AcceptOrDenyInvitationState {
  accepted: boolean;
  loading: boolean;
  success: boolean;
  error: any;
}

export const acceptOrDenyInvitationInitialState: AcceptOrDenyInvitationState = {
  accepted: false,
  loading: false,
  success: false,
  error: null,
};

export const AcceptOrDenyInvitationStateReducer = createReducer(
  acceptOrDenyInvitationInitialState,
  on(JobSpaceAction.acceptOrDenyInvitationRequests, (state) => ({...state, loading: true})),
  on(JobSpaceAction.acceptOrDenyInvitationSuccess, (state, {accepted}) => ({
    ...state,
    accepted,
    loading: false,
    error: null,
    success: true
  })),
  on(JobSpaceAction.addUserToJobSpaceRequestsFailure, (state, {error}) => ({...state, loading: false, error}))
);

export interface GetUserJobSpacesState {
  jobSpaces: JobSpaceModel[];
  loading: boolean;
  success: boolean;
  error: any;
}

export const getUserJobSpacesInitialState: GetUserJobSpacesState = {
  jobSpaces: [],
  loading: false,
  success: false,
  error: null,
};

export const getUserJobSpacesStateReducer = createReducer(
  getUserJobSpacesInitialState,
  on(JobSpaceAction.getUserJobSpacesRequests, (state) => ({...state, loading: true})),
  on(JobSpaceAction.getUserJobSpacesSuccess, (state, {jobSpaces}) => ({
    ...state,
    jobSpaces: jobSpaces,
    loading: false,
    error: null,
    success: true
  })),
  on(JobSpaceAction.getUserJobSpacesFailure, (state, {error}) => ({...state, loading: false, error}))
);
