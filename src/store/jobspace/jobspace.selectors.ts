import {createFeatureSelector, createSelector} from '@ngrx/store';
import {
  AcceptOrDenyInvitationState,
  AddUserToJobSpaceState,
  CreateJobSpaceState,
  GetUserJobSpacesState,
  JobSpaceState
} from './jobspace.reducer';

export const selectJobSpaceState = createFeatureSelector<JobSpaceState>('jobSpaceModel');

export const selectJobSpaceModel = createSelector(
  selectJobSpaceState,
  (state: JobSpaceState) => state.jobSpaceModel
);
export const selectAcceptOrDenyJobSpaceState = createFeatureSelector<AcceptOrDenyInvitationState>('jobSpaceModel');

export const selectAcceptedModel = createSelector(
  selectAcceptOrDenyJobSpaceState,
  (state: AcceptOrDenyInvitationState) => state.accepted
);

export const selectCreateJobSpaceState = createFeatureSelector<CreateJobSpaceState>('jobSpaceModel');

export const selectCreateJobSpace = createSelector(
  selectCreateJobSpaceState,
  (state: CreateJobSpaceState) => state.jobSpaceModel
);

export const selectAddUserToJobSpaceState = createFeatureSelector<AddUserToJobSpaceState>('jobSpaceModel');

export const selectAddUserToJobSpace = createSelector(
  selectAddUserToJobSpaceState,
  (state: AddUserToJobSpaceState) => state.jobSpaceModel
);

export const selectGetUserJobSpacesState = createFeatureSelector<GetUserJobSpacesState>('jobSpaces');

export const selectGetUserJobSpaces = createSelector(
  selectGetUserJobSpacesState,
  (state: GetUserJobSpacesState) => state.jobSpaces
);
