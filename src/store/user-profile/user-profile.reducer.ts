import {createReducer, on} from '@ngrx/store';
import * as UserProfileActions from 'src/store/user-profile/user-profile.actions';
import {UserProfileModel} from "../../domain/model/user-profile.model";

export interface UserProfileState {
    userProfile: UserProfileModel;
    loading: boolean;
    success: boolean;
    error: any;
}

export const initialState: UserProfileState = {
    userProfile: {
        firstName: '',
        lastName: '',
        phoneNumber: '',
        country: '',
        governance: '',
        user: {
            id: '',
            username: '',
            email: '',
            token: ''
        },
        occupation: {
            id: '',
            name: ''
        },
        skills: [],
        userImage: '',
        id: ''
    },
    loading: false,
    success: false,
    error: null,
};

export const userProfileReducer = createReducer(
    initialState,
    on(UserProfileActions.addUserProfile, (state) => ({...state, loading: true})),
    on(UserProfileActions.addUserProfileSuccess, (state, {userProfile}) => ({
        ...state,
        userProfile,
        loading: false,
        error: null,
        success: true
    })),
    on(UserProfileActions.addUserProfileFailure, (state, {error}) => ({...state, loading: false, error})),
    on(UserProfileActions.getUserProfile, (state) => ({...state, loading: true})),
    on(UserProfileActions.getUserProfileSuccess, (state, {userProfile}) => ({
        ...state,
        userProfile,
        loading: false,
        error: null,
        success: true
    })),
    on(UserProfileActions.getUserProfileFailure, (state, {error}) => ({...state, loading: false, error}))
);


export interface SearchState {
    userProfiles: UserProfileModel[];
    loading: boolean;
    success: boolean;
    error: any;
}

export const initialSearchState: SearchState = {
    userProfiles: [],
    loading: false,
    success: false,
    error: null,
};

export const searchReducer = createReducer(
    initialSearchState,
    on(UserProfileActions.search, (state) => ({...state, loading: true})),
    on(UserProfileActions.searchSuccess, (state, {userProfiles}) => {
        console.log('UserProfile added:', userProfiles); // Log the userProfile
        return {
            ...state,
            userProfiles,
            loading: false,
            error: null,
            success: true
        };
    }),
    on(UserProfileActions.searchFailure, (state, {error}) => ({...state, loading: false, error}))
);

export interface ProfileByIdState {
  profile: UserProfileModel;
  loading: boolean;
  success: boolean;
  error: any;
}

export const initialProfileByIdState: ProfileByIdState = {
  profile: {
    id: '',
    firstName: '',
    lastName: '',
    phoneNumber: '',
    country: '',
    governance: '',
    userImage: '',
    user: {
      id: '',
      username: '',
      email: '',
      token: ''
    },
    occupation: {
      id: '',
      name: ''
    },
    skills: []
  },
  loading: false,
  success: false,
  error: null,
};

export const getProfileByIdReducer = createReducer(
  initialProfileByIdState,
  on(UserProfileActions.profileById, (state) => ({...state, loading: true})),
  on(UserProfileActions.profileByIdSuccess, (state, {profile}) => {
    console.log('UserProfile added:', profile); // Log the userProfile
    return {
      ...state,
      profile,
      loading: false,
      error: null,
      success: true
    };
  }),
  on(UserProfileActions.profileByIdFailure, (state, {error}) => ({...state, loading: false, error}))
);
