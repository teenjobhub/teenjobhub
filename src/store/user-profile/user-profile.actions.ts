// skill.actions.ts
import {createAction, props} from '@ngrx/store';
import {UserProfileModel} from "../../domain/model/user-profile.model";

export const addUserProfile = createAction('[UserProfile] Add UserProfile', props<{ userProfile: UserProfileModel }>());
export const addUserProfileSuccess = createAction('[UserProfile] Add UserProfile Success', props<{
  userProfile: UserProfileModel
}>());
export const addUserProfileFailure = createAction('[UserProfile] Add UserProfile Failure', props<{ error: any }>());

export const getUserProfile = createAction('[UserProfile] Load UserProfile', props<{ userID: string }>());
export const getUserProfileSuccess = createAction('[UserProfile] Load UserProfile Success', props<{
  userProfile: UserProfileModel
}>());
export const getUserProfileFailure = createAction('[UserProfile] Load UserProfile Failure', props<{ error: any }>());

export const search = createAction('[Search] Load result', props<{ query: string }>());
export const searchSuccess = createAction('[Search] Load result Success', props<{
  userProfiles: UserProfileModel[]
}>());
export const searchFailure = createAction('[Search] Load result Failure', props<{ error: any }>());

export const profileById = createAction('[Search] Load profile ', props<{ userProfileId: string }>());
export const profileByIdSuccess = createAction('[Search] Load profile Success', props<{
  profile: UserProfileModel
}>());
export const profileByIdFailure = createAction('[Search] Load profile Failure', props<{ error: any }>());
