import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {of, tap} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import * as UserProfileActions from 'src/store/user-profile/user-profile.actions';
import {
  UserProfileImplementationRepository
} from "../../domain/repositories/user-profile/user-profile-implementation.repository";

@Injectable()
export class UserProfileEffects {
  addUserProfile$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserProfileActions.addUserProfile),
      switchMap((action) =>
        this.userProfileImplementationRepository.addUserProfile(action.userProfile).pipe(
          map((userProfile) => UserProfileActions.addUserProfileSuccess({userProfile})),
          catchError((error) => of(UserProfileActions.addUserProfileFailure({error})))
        )
      )
    )
  );
  getUserProfile$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserProfileActions.getUserProfile),
      switchMap((action) =>
        this.userProfileImplementationRepository.getUserProfile().pipe(
          map((userProfile) => UserProfileActions.getUserProfileSuccess({userProfile})),
          catchError((error) => of(UserProfileActions.getUserProfileFailure({error})))
        )
      )
    )
  );
    search$ = createEffect(() =>
        this.actions$.pipe(
            ofType(UserProfileActions.search),
            switchMap((action) =>
                this.userProfileImplementationRepository.search(action.query).pipe(
                    map((userProfile) => UserProfileActions.searchSuccess   ({userProfiles:userProfile})),
                    catchError((error) => of(UserProfileActions.searchFailure({error})))
                )
            )
        )
    );

  profileById$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserProfileActions.profileById),
      switchMap((action) =>
        this.userProfileImplementationRepository.findById(action.userProfileId).pipe(
          map((userProfile) => UserProfileActions.profileByIdSuccess({profile:userProfile})),
          catchError((error) => of(UserProfileActions.profileByIdFailure({error})))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private userProfileImplementationRepository: UserProfileImplementationRepository
  ) {
  }

}
