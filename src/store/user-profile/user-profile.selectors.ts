import {createFeatureSelector, createSelector} from '@ngrx/store';
import {ProfileByIdState, SearchState, UserProfileState} from "./user-profile.reducer";
import {SearchUsecase} from "../../domain/usecases/user/search-usecase";

export const selectUserProfileState = createFeatureSelector<UserProfileState>('userProfile');

export const selectSkills = createSelector(
  selectUserProfileState,
  (state: UserProfileState) => state.userProfile
);
export const selectSearchState = createFeatureSelector<SearchState>('userProfiles');

export const selectSearchResults = createSelector(
    selectSearchState,
    (state: SearchState) => state.userProfiles
);
export const selectUserProfileByIdState = createFeatureSelector<ProfileByIdState>('profile');

export const selectUserProfileById = createSelector(
  selectUserProfileByIdState,
  (state: ProfileByIdState) => state.profile
);
