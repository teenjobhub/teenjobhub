import {createAction, props} from '@ngrx/store';
import {ChatMessageModel} from "../../domain/model/chat-message.model";
import {ChatRoomModel} from "../../domain/model/chat-room.model";

export const getMessages = createAction('[Chat] GetMessages',props<{
  chatRoomId: string
}>());
export const getMessagesSuccess = createAction('[Chat] GetMessages Success', props<{
  messages: ChatMessageModel[]
}>());
export const getMessagesFailure = createAction('[Connection] GetMessages Failure', props<{
  error: any
}>());

export const getChatRoom = createAction('[Chat] GetChatRoom', props<{
  chatRoomOrUserProfileId: string
}>());
export const getChatRoomSuccess = createAction('[Chat] GetChatRoom Success', props<{
  chatRoom: ChatRoomModel
}>());
export const getChatRoomFailure = createAction('[Chat] GetChatRoom Failure', props<{
  error: any
}>());

export const sendMessage = createAction('[Chat] SendMessage', props<{
  chatRoomId: string,message: string}>());
export const sendMessageSuccess = createAction('[Chat] SendMessage Success', props<{
  sent: boolean
}>());
export const sendMessageFailure = createAction('[Chat] SendMessage Failure', props<{
  error: any
}>());

export const getChatRooms = createAction('[Chat] GetChatRooms', props<{
  userProfileId: string
}>());
export const getChatRoomsSuccess = createAction('[Chat] GetChatRooms Success', props<{
  chatRooms: ChatRoomModel[]
}>());
export const getChatRoomsFailure = createAction('[Chat] GetChatRooms Failure', props<{
  error: any
}>());
