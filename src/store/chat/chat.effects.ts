import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import * as ChatAction from 'src/store/chat/chat.actions';
import {GetChatRoomUsecase} from "../../domain/usecases/chat/get-chat-room.usecase";
import {GetMessagesUsecase} from "../../domain/usecases/chat/get-messages.usecase";
import {SendMessageUsecase} from "../../domain/usecases/chat/send-message.usecase";
import {GetChatRoomsUsecase} from "../../domain/usecases/chat/get-chat-rooms.usecase";


@Injectable()
export class ChatEffects {
  getChatRoom$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ChatAction.getChatRoom),
      switchMap((action) =>
        this.getChatRoomUsecase.execute(action.chatRoomOrUserProfileId).pipe(
          map((chatRoomModel) => ChatAction.getChatRoomSuccess({chatRoom: chatRoomModel})),
          catchError((error) => of(ChatAction.getChatRoomFailure({error})))
        )
      )
    )
  );

  getChatRooms$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ChatAction.getChatRooms),
      switchMap((action) =>
        this.getChatRoomsUsecase.execute(action.userProfileId).pipe(
          map((chatRoomModel) => ChatAction.getChatRoomsSuccess({chatRooms: chatRoomModel})),
          catchError((error) => of(ChatAction.getChatRoomsFailure({error})))
        )
      )
    )
  );

  getMessages$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ChatAction.getMessages),
      switchMap((action) =>
        this.getMessagesUsecase.execute(action.chatRoomId).pipe(
          map((messages) => ChatAction.getMessagesSuccess({messages: messages})),
          catchError((error) => of(ChatAction.getMessagesFailure({error})))
        )
      )
    )
  );

  sendMessage = createEffect(() =>
    this.actions$.pipe(
      ofType(ChatAction.sendMessage),
      switchMap((action) =>
        this.sendMessageUsecase.execute(action).pipe(
          map((sent
          ) => ChatAction.sendMessageSuccess({sent: sent})),
          catchError((error) => of(ChatAction.sendMessageFailure({error})))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private getChatRoomUsecase: GetChatRoomUsecase,
    private getChatRoomsUsecase: GetChatRoomsUsecase,
    private getMessagesUsecase: GetMessagesUsecase,
    private sendMessageUsecase: SendMessageUsecase,
  ) {
  }
}
