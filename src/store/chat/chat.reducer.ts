import {createReducer, on} from '@ngrx/store';
import * as ChatAction from 'src/store/chat/chat.actions';
import {ChatRoomModel} from "../../domain/model/chat-room.model";
import {ChatMessageModel} from "../../domain/model/chat-message.model";

export interface GetChatRoomState {
  chatRoom?: ChatRoomModel;
  loading: boolean;
  success: boolean;
  error: any;
}

export const initialGetChatRoomState: GetChatRoomState = {
  chatRoom: undefined,
  loading: false,
  success: false,
  error: null,
};

export const chatRoomReducer = createReducer(
  initialGetChatRoomState,
  on(ChatAction.getChatRoom, (state) => ({...state, loading: true})),
  on(ChatAction.getChatRoomSuccess, (state, {chatRoom}) => ({
    ...state,
    chatRoom,
    loading: false,
    error: null,
    success: true
  })),
  on(ChatAction.getChatRoomFailure, (state, {error}) => ({...state, loading: false, error}))
);

export interface GetChatRoomsState {
  chatRooms: ChatRoomModel[];
  loading: boolean;
  success: boolean;
  error: any;
}

export const initialGetChatRoomsState: GetChatRoomsState = {
  chatRooms: [],
  loading: false,
  success: false,
  error: null,
};

export const chatRoomsReducer = createReducer(
  initialGetChatRoomsState,
  on(ChatAction.getChatRooms, (state) => ({...state, loading: true})),
  on(ChatAction.getChatRoomsSuccess, (state, {chatRooms}) => ({
    ...state,
    chatRooms,
    loading: false,
    error: null,
    success: true
  })),
  on(ChatAction.getChatRoomsFailure, (state, {error}) => ({...state, loading: false, error}))
);

export interface GetMessagesState {
  messages: ChatMessageModel[];
  loading: boolean;
  success: boolean;
  error: any;
}

export const initialGetMessagesState: GetMessagesState = {
  messages: [],
  loading: false,
  success: false,
  error: null,
};

export const messagesReducer = createReducer(
  initialGetMessagesState,
  on(ChatAction.getMessages, (state) => ({...state, loading: true})),
  on(ChatAction.getMessagesSuccess, (state, {messages}) => ({
    ...state,
    messages,
    loading: false,
    error: null,
    success: true
  })),
  on(ChatAction.getMessagesFailure, (state, {error}) => ({...state, loading: false, error}))
);

export interface SendMessagesState {
  sent: boolean;
  loading: boolean;
  success: boolean;
  error: any;
}

export const initialSendMessagesState: SendMessagesState = {
  sent: false,
  loading: false,
  success: false,
  error: null,
};

export const sendMessagesReducer = createReducer(
  initialSendMessagesState,
  on(ChatAction.sendMessage, (state) => ({...state, loading: true})),
  on(ChatAction.sendMessageSuccess, (state, {sent}) => ({
    ...state,
    sent,
    loading: false,
    error: null,
    success: true
  })),
  on(ChatAction.sendMessageFailure, (state, {error}) => ({...state, loading: false, error}))
);
