import {createFeatureSelector, createSelector} from '@ngrx/store';
import {GetChatRoomsState, GetChatRoomState, GetMessagesState, SendMessagesState,} from './chat.reducer';
import {GetChatRoomsUsecase} from "../../domain/usecases/chat/get-chat-rooms.usecase";

export const selectGetMessagesState = createFeatureSelector<GetMessagesState>('messages');

export const selectGetMessages = createSelector(
  selectGetMessagesState,
  (state: GetMessagesState) => state.messages
);

export const selectGetChatRoomState = createFeatureSelector<GetChatRoomState>('chatRooms');

export const selectGetChatRoom = createSelector(
  selectGetChatRoomState,
  (state: GetChatRoomState) => state.chatRoom
);

export const selectGetChatRoomsState = createFeatureSelector<GetChatRoomsState>('chatRooms');

export const selectGetChatRooms = createSelector(
  selectGetChatRoomsState,
  (state: GetChatRoomsState) => state.chatRooms
);

export const selectSendMessagesState = createFeatureSelector<SendMessagesState>('chatRooms');

export const selectSendMessages = createSelector(
  selectSendMessagesState,
  (state: SendMessagesState) => state.sent
);
