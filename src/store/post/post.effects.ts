import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import * as PostAction from 'src/store/post/post.actions';

import {AddPostUseCase} from "../../domain/usecases/posts/add-post.usecase";
import {GetPostUseCase} from "../../domain/usecases/posts/get-post.usecase";
import {GetJobsByUserUsecase} from "../../domain/usecases/posts/get-by-user.usecase";
import {ApplyUsecase} from "../../domain/usecases/posts/apply.usecase";
import {HireUsecase} from "../../domain/usecases/posts/hire.usecase";
import {MyApplicationsUsecase} from "../../domain/usecases/posts/my-applications.usecase";
import {RateUsecase} from "../../domain/usecases/posts/rate.usecase";

@Injectable()
export class PostEffects {
  addPost = createEffect(() =>
    this.actions$.pipe(
      ofType(PostAction.addPost),
      switchMap((action) =>
        this.addPostUseCase.execute(action).pipe(
          map((post) => PostAction.addPostSuccess({post})),
          catchError((error) => of(PostAction.addPostFailure({error})))
        )
      )
    )
  );
  getPost = createEffect(() =>
    this.actions$.pipe(
      ofType(PostAction.getPost),
      switchMap((action) =>
        this.getPostUseCase.execute().pipe(
          map((post) => PostAction.getPostSuccess({post})),
          catchError((error) => of(PostAction.addPostFailure({error})))
        )
      )
    )
  );

  getPostByUser = createEffect(() =>
    this.actions$.pipe(
      ofType(PostAction.getPostByUser),
      switchMap((action) =>
        this.getJobsByUserUsecase.execute(action.closed).pipe(
          map((posts) => PostAction.getPostByUserSuccess({posts: posts})),
          catchError((error) => of(PostAction.getPostByUserFailure({error})))
        )
      )
    )
  );

  applyForJob = createEffect(() =>
    this.actions$.pipe(
      ofType(PostAction.apply),
      switchMap((action) =>
        this.applyUsecase.execute(action.jobPostID).pipe(
          map((applied) => PostAction.applySuccess({applied})),
          catchError((error) => of(PostAction.applyFailure({error})))
        )
      )
    )
  );
  hiredForJob = createEffect(() =>
    this.actions$.pipe(
      ofType(PostAction.hire),
      switchMap((action) =>
        this.hireUsecase.execute({jobPostID: action.jobPostID, userProfileID: action.userProfileID}).pipe(
          map((message) => PostAction.hireSuccess({hired:message.message})),
          catchError((error) => of(PostAction.hireFailure({error})))
        )
      )
    )
  );

  myApplications = createEffect(() =>
    this.actions$.pipe(
      ofType(PostAction.myApplications),
      switchMap((action) =>
        this.myApplicationUseCase.execute().pipe(
          map((myApplications) => PostAction.myApplicationsSuccess({myApplications})),
          catchError((error) => of(PostAction.myApplicationsFailure({error})))
        )
      )
    )
  );

  rate = createEffect(() =>
    this.actions$.pipe(
      ofType(PostAction.rate),
      switchMap((action) =>
        this.rateUseCase.execute({jobPostID:action.jobPostId,rate:action.rate}).pipe(
          map((rate) => PostAction.rateSuccess({rate})),
          catchError((error) => of(PostAction.rateFailure({error})))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private addPostUseCase: AddPostUseCase,
    private getPostUseCase: GetPostUseCase,
    private getJobsByUserUsecase: GetJobsByUserUsecase,
    private applyUsecase: ApplyUsecase,
    private hireUsecase: HireUsecase,
    private myApplicationUseCase: MyApplicationsUsecase,
    private rateUseCase: RateUsecase,
  ) {
  }
}
