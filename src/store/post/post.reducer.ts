import {createReducer, on} from '@ngrx/store';
import * as PostAction from "src/store/post/post.actions"
import {JobPostModel} from "../../domain/model/job-post.model";

export interface PostState {
  post: JobPostModel;
  loading: boolean;
  error: any;
}

export interface JobPostState {
  jobPost: JobPostModel[];
  loading: boolean;
  error: any;
}

export const initialState: PostState = {
  post: {
    jobPostTitle: '',
    id: '',
    jobPostDescription: '',
    postDate: '',
    postImageUrl: '',
    user: {
      id: '',
      firstName: '',
      lastName: '',
      phoneNumber: '',
      country: '',
      governance: '',
      userImage: '',
      user: {
        id: '',
        username: '',
        email: '',
        token: ''
      },
      occupation: {
        id: '',
        name: ''
      },
      skills: []
    },
    comments: [],
    jobSpace: {},
    applicants: [],
    open: false,
    hiredUser: {
      id: '',
      firstName: '',
      lastName: '',
      phoneNumber: '',
      country: '',
      governance: '',
      userImage: '',
      user: {
        id: '',
        username: '',
        email: '',
        token: ''
      },
      occupation: {
        id: '',
        name: ''
      },
      skills: []
    }
  },
  loading: false,
  error: null,
};

export const initialJobState: JobPostState = {
  jobPost: [],
  loading: false,
  error: null,
};

export const postReducer = createReducer(
  initialState,
  on(PostAction.addPost, (state) => ({...state, loading: true})),
  on(PostAction.addPostSuccess, (state, {post}) => {

    return ({
      ...state,
      post: post,
      loading: false,
      error: null
    })
  }),
  on(PostAction.addPostFailure, (state, {error}) => ({...state, loading: false, error})),
);
export const jobPostReducer = createReducer(
  initialJobState,
  on(PostAction.getPost, (state) => ({...state, loading: true})),
  on(PostAction.getPostSuccess, (state, {post}) => {
    const posts = [...post]
    posts.sort((a, b) => {
      // Convert postDate to Date object for comparison
      const dateA = new Date(a.postDate);
      const dateB = new Date(b.postDate);

      // Compare dates: most recent first
      return dateB.getTime() - dateA.getTime();
    });
    return {
      ...state,
      jobPost: posts,
      loading: false,
      error: null
    };
  }),
  on(PostAction.getPostFailure, (state, {error}) => ({...state, loading: false, error})),
);

export interface GetJobsByUserState {
  posts: JobPostModel[];
  loading: boolean;
  error: any;
}

export const initialGetJobsByUserState: GetJobsByUserState = {
  posts: [],
  loading: false,
  error: null,
};
export const getJobsByUserReducer = createReducer(
  initialGetJobsByUserState,
  on(PostAction.getPostByUser, (state) => ({...state, loading: true})),
  on(PostAction.getPostByUserSuccess, (state, {posts}) => {
    return {
      ...state,
      posts: posts,
      loading: false,
      error: null
    };
  }),
  on(PostAction.getPostByUserFailure, (state, {error}) => ({...state, loading: false, error})),
);

export interface ApplyState {
  applied: boolean;
  loading: boolean;
  error: any;
}

export const initialAppliedState: ApplyState = {
  applied: false,
  loading: false,
  error: null,
};
export const appliedReducer = createReducer(
  initialAppliedState,
  on(PostAction.apply, (state) => ({...state, loading: true})),
  on(PostAction.applySuccess, (state, {applied}) => {

    return {
      ...state,
      applied: applied,
      loading: false,
      error: null
    };
  }),
  on(PostAction.applyFailure, (state, {error}) => ({...state, loading: false, error})),
);

export interface HiredState {
  hired: string;
  loading: boolean;
  error: any;
}

export const initialHiredState: HiredState = {
  hired: "",
  loading: false,
  error: null,
};
export const hiredReducer = createReducer(
  initialHiredState,
  on(PostAction.hire, (state) => ({...state, loading: true})),
  on(PostAction.hireSuccess, (state, {hired}) => {

    return {
      ...state,
      hired: hired,
      loading: false,
      error: null
    };
  }),
  on(PostAction.applyFailure, (state, {error}) => ({...state, loading: false, error})),
);

export interface MyApplicationsState {
  myApplications: JobPostModel[];
  loading: boolean;
  error: any;
}

export const initialMyApplicationsState: MyApplicationsState = {
  myApplications: [],
  loading: false,
  error: null,
};
export const myApplicationsReducer = createReducer(
  initialMyApplicationsState,
  on(PostAction.myApplications, (state) => ({...state, loading: true})),
  on(PostAction.myApplicationsSuccess, (state, {myApplications}) => {

    return {
      ...state,
      myApplications: myApplications,
      loading: false,
      error: null
    };
  }),
  on(PostAction.myApplicationsFailure, (state, {error}) => ({...state, loading: false, error})),
);

export interface RateState {
  rate: boolean;
  loading: boolean;
  error: any;
}

export const initialRateState: RateState = {
  rate: false ,
  loading: false,
  error: null,
};
export const rateReducer = createReducer(
  initialRateState,
  on(PostAction.rate, (state) => ({...state, loading: true})),
  on(PostAction.rateSuccess, (state, {rate}) => {

    return {
      ...state,
      rate: rate,
      loading: false,
      error: null
    };
  }),
  on(PostAction.rateFailure, (state, {error}) => ({...state, loading: false, error})),
);
