import {createFeatureSelector, createSelector} from '@ngrx/store';
import {
  ApplyState,
  GetJobsByUserState,
  HiredState,
  JobPostState,
  MyApplicationsState,
  PostState,
  RateState
} from "./post.reducer";
import {MyApplicationsUsecase} from "../../domain/usecases/posts/my-applications.usecase";

export const selectPostState = createFeatureSelector<PostState>('post');

export const selectPosts = createSelector(
  selectPostState,
  (state: PostState) => state.post
);

export const selectJobPostState = createFeatureSelector<JobPostState>('jobPost');

export const selectJobPosts = createSelector(
  selectJobPostState,
  (state: JobPostState) => state.jobPost
);
export const selectJobPostByUserState = createFeatureSelector<GetJobsByUserState>('posts');

export const selectJobPostsByUser = createSelector(
  selectJobPostByUserState,
  (state: GetJobsByUserState) => state.posts
);
export const selectApplicationState = createFeatureSelector<ApplyState>('Apply');

export const selectJobApplication = createSelector(
  selectApplicationState,
  (state: ApplyState) => state.applied
);
export const selectHiredState = createFeatureSelector<HiredState>('hired');

export const selectHiredApplication = createSelector(
  selectHiredState,
  (state: HiredState) => state.hired
);

export const selectMyApplicationsState = createFeatureSelector<MyApplicationsState>('myApplications');

export const selectMyApplications = createSelector(
  selectMyApplicationsState,
  (state: MyApplicationsState) => state.myApplications
);


export const selectRateState = createFeatureSelector<RateState>('rate');

export const selectRate = createSelector(
  selectRateState,
  (state: RateState) => state.rate
);
