import {createAction, props} from '@ngrx/store';
import {AddPostModel} from "../../domain/model/add-post.model";
import {JobPostModel} from "../../domain/model/job-post.model";

export const addPost = createAction('[post] AddPosts', props<{ postsModel: AddPostModel }>());
export const addPostSuccess = createAction('[post] Add Posts Success', props<{
  post: JobPostModel
}>());
export const addPostFailure = createAction('[post] Add Posts Failure', props<{ error: any }>());

export const getPost = createAction('[post] GetPosts');
export const getPostSuccess = createAction('[post] Get Posts Success', props<{
  post: JobPostModel[]
}>());
export const getPostFailure = createAction('[post] Get Posts Failure', props<{ error: any }>());

export const getPostByUser = createAction('[post] GetPostsByUser', props<{ closed: boolean }>());
export const getPostByUserSuccess = createAction('[post] GetPostsByUser ', props<{
  posts: JobPostModel[]
}>());
export const getPostByUserFailure = createAction('[post] GetPostsByUser ', props<{ error: any }>());

export const apply = createAction('[post] Apply', props<{ jobPostID: string }>());
export const applySuccess = createAction('[post] Apply Success', props<{
  applied: boolean
}>());
export const applyFailure = createAction('[post] Apply Failure', props<{ error: any }>());

export const hire = createAction('[post] Hire', props<{ jobPostID: string, userProfileID: string }>());
export const hireSuccess = createAction('[post] Hire Success', props<{
  hired: string
}>());
export const hireFailure = createAction('[post] Hire Failure', props<{ error: any }>());

export const myApplications = createAction('[post] Mire');
export const myApplicationsSuccess = createAction('[post] MyApplications Success', props<{
  myApplications: JobPostModel[]
}>());
export const myApplicationsFailure = createAction('[post] MyApplications Failure', props<{ error: any }>());

export const rate = createAction('[post] Rate',props<{
  jobPostId: string,
  rate:number
}>());
export const rateSuccess = createAction('[post] Rate Success', props<{
  rate: boolean
}>());
export const rateFailure = createAction('[post] Rate Failure', props<{ error: any }>());
