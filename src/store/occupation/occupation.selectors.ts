import {createFeatureSelector, createSelector} from '@ngrx/store';
import {OccupationState} from "./occupation.reducer";

export const selectOccupationState = createFeatureSelector<OccupationState>('Occupations');

export const selectOccupations = createSelector(
  selectOccupationState,
  (state: OccupationState) => state.Occupations
);
