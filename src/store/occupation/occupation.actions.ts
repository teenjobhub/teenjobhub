import {createAction, props} from '@ngrx/store';
import {OccupationModel} from "../../domain/model/occupation.model";

export const loadOccupations = createAction('[Occupations] Load Occupations');
export const loadOccupationsSuccess = createAction('[Occupations] Load Occupations Success', props<{
  Occupations: OccupationModel[]
}>());
export const loadOccupationsFailure = createAction('[Occupations] Load Occupations Failure', props<{ error: any }>());
