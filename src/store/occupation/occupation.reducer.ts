import {createReducer, on} from '@ngrx/store';
import {OccupationModel} from "../../domain/model/occupation.model";
import * as OccupationActions from "src/store/occupation/occupation.actions"

export interface OccupationState {
  Occupations: OccupationModel[];
  loading: boolean;
  error: any;
}

export const initialState: OccupationState = {
  Occupations: [],
  loading: false,
  error: null,
};

export const occupationReducer = createReducer(
  initialState,
  on(OccupationActions.loadOccupations, (state) => ({...state, loading: true})),
  on(OccupationActions.loadOccupationsSuccess, (state, {Occupations}) => ({
    ...state,
    Occupations,
    loading: false,
    error: null
  })),
  on(OccupationActions.loadOccupationsFailure, (state, {error}) => ({...state, loading: false, error}))
);
