import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import * as OccupationActions from 'src/store/occupation/occupation.actions';
import {GetOccupationUsecase} from "../../domain/usecases/occupation/get-occupation.usecase";

@Injectable()
export class OccupationEffects {
  loadOccupations$ = createEffect(() =>
    this.actions$.pipe(
      ofType(OccupationActions.loadOccupations),
      switchMap(() =>
        this.getOccupationUsecase.execute().pipe(
          map((Occupations) => OccupationActions.loadOccupationsSuccess({Occupations})),
          catchError((error) => of(OccupationActions.loadOccupationsFailure({error})))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private getOccupationUsecase: GetOccupationUsecase
  ) {
  }
}
