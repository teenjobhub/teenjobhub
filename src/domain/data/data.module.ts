import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {UserLoginUseCase} from 'src/domain/usecases/user/user-login.usecase';
import {GetUserProfileUseCase} from 'src/domain/usecases/user/get-user-profile.usecase';
import {UserRepository} from "../repositories/user/user.repository";
import {UserImplementationRepository} from "../repositories/user/user-implementation.repository";
import {UserRegisterUseCase} from "../usecases/user/user-register.usecase";
import {UserProfileRepository} from "../repositories/user-profile/user-profile.repository";
import {UserProfileImplementationRepository} from "../repositories/user-profile/user-profile-implementation.repository";
import {InsertUserProfileUseCase} from "../usecases/user/insert-user-profile.usecase";
import {SkillsRepository} from "../repositories/skills/skills.repository";
import {GetSkillsUsecase} from "../usecases/skills/get-skills.usecase";
import {SkillsImplementationRepository} from "../repositories/skills/skills-implementation.repository";
import {OccupationRepository} from "../repositories/occupation/occupation.repository";
import {GetOccupationUsecase} from "../usecases/occupation/get-occupation.usecase";
import {OccupationImplementationRepository} from "../repositories/occupation/occupation-implementation.repository";
import {PostsRepository} from "../repositories/posts/posts.repository";
import {AddPostUseCase} from "../usecases/posts/add-post.usecase";
import {PostsImplementationRepository} from "../repositories/posts/posts-implementation.repository";
import {GetPostUseCase} from "../usecases/posts/get-post.usecase";
import {UpdateUserImageUseCase} from "../usecases/user/update-user-image.usecase";
import {ConnectionRepository} from "../repositories/connections/connection.repository";
import {GetPendingConnectionsUsecase} from "../usecases/connection/get-pending-connections.usecase";
import {ConnectionImplementationRepository} from "../repositories/connections/connection-implementation.repository";
import {JobspaceRepository} from "../repositories/jobspace/jobspace.repository";
import {GetPendingInvitationsUsecase} from "../usecases/jobspace/get-pending-invitations.usecase";
import {JobspaceImplementationRepository} from "../repositories/jobspace/jobspace-implementation.repository";
import {AcceptordenyConnectionUsecase} from "../usecases/connection/acceptordeny-connection.usecase";
import {GetUserConnectionsUsecase} from "../usecases/connection/get-user-connections.usecase";
import {AddConnectionUsecase} from "../usecases/connection/add-connection.usecase";
import {CreateJobspaceUsecase} from "../usecases/jobspace/create-jobspace-usecase";
import {AddUserToJobspaceUsecase} from "../usecases/jobspace/add-user-to-jobspace.usecase";
import {AcceptOrDenyJobSpaceInvitationUsecase} from "../usecases/jobspace/accept-or-deny-invitation.usecase";
import {GetUserJobspaces} from "../usecases/jobspace/get-user-jobspaces";
import {GetJobsByUserUsecase} from "../usecases/posts/get-by-user.usecase";
import {ApplyUsecase} from "../usecases/posts/apply.usecase";
import {HireUsecase} from "../usecases/posts/hire.usecase";
import {NotificationRepository} from "../repositories/notification/notification.repository";
import {GetNotificationUsecase} from "../usecases/notification/get-notification.usecase";
import {NotificationImplementation} from "../repositories/notification/notification-implementation";
import {GetSuggestionsUsecase} from "../usecases/connection/get-suggestions.usecase";
import {SearchUsecase} from "../usecases/user/search-usecase";
import {UpdateUserSignatureUsecase} from "../usecases/user/update-user-signature.usecase";
import {ChatRepository} from "../repositories/chat/chat.repository";
import {GetMessagesUsecase} from "../usecases/chat/get-messages.usecase";
import {GetChatRoomUsecase} from "../usecases/chat/get-chat-room.usecase";
import {SendMessageUsecase} from "../usecases/chat/send-message.usecase";
import {ChatImplementationRepository} from "../repositories/chat/chat-implementation.repository";
import {GetChatRoomsUsecase} from "../usecases/chat/get-chat-rooms.usecase";
import {MyApplicationsUsecase} from "../usecases/posts/my-applications.usecase";
import {RateUsecase} from "../usecases/posts/rate.usecase";

const userLoginUseCaseFactory =
  (userRepo: UserRepository) => new UserLoginUseCase(userRepo);
export const userLoginUseCaseProvider = {
  provide: UserLoginUseCase,
  useFactory: userLoginUseCaseFactory,
  deps: [UserRepository],
};
const userRegisterUseCaseFactory =
  (userRepo: UserRepository) => new UserRegisterUseCase(userRepo);
export const userRegisterUseCaseProvider = {
  provide: UserRegisterUseCase,
  useFactory: userRegisterUseCaseFactory,
  deps: [UserRepository],
};
const getUserProfileUseCaseFactory =
  (userProfileRepo: UserProfileRepository) => new GetUserProfileUseCase(userProfileRepo);
export const getUserProfileUseCaseProvider = {
  provide: GetUserProfileUseCase,
  useFactory: getUserProfileUseCaseFactory,
  deps: [UserProfileRepository],
};

const addUserProfileUseCaseFactory =
  (userProfileRepo: UserProfileRepository) => new InsertUserProfileUseCase(userProfileRepo);
export const addUserProfileUseCaseProvider = {
  provide: InsertUserProfileUseCase,
  useFactory: addUserProfileUseCaseFactory,
  deps: [UserProfileRepository],
};

const updateUserProfileImageUseCaseFactory =
  (userProfileRepo: UserProfileRepository) => new UpdateUserImageUseCase(userProfileRepo);
export const updateUserProfileImageUseCaseProvider = {
  provide: UpdateUserImageUseCase,
  useFactory: updateUserProfileImageUseCaseFactory,
  deps: [UserProfileRepository],
};

const updateUserSignatureUseCaseFactory =
  (userProfileRepo: UserProfileRepository) => new UpdateUserSignatureUsecase(userProfileRepo);
export const updateUserSignatureUseCaseProvider = {
  provide: UpdateUserSignatureUsecase,
  useFactory: updateUserSignatureUseCaseFactory,
  deps: [UserProfileRepository],
};

const searchUseCaseFactory =
  (userProfileRepo: UserProfileRepository) => new SearchUsecase(userProfileRepo);
export const searchUseCaseProvider = {
  provide: SearchUsecase,
  useFactory: searchUseCaseFactory,
  deps: [UserProfileRepository],
};


const getSkillsUseCaseFactory =
  (skillsRepository: SkillsRepository) => new GetSkillsUsecase(skillsRepository);
export const getSkillsUseCaseProvider = {
  provide: GetSkillsUsecase,
  useFactory: getSkillsUseCaseFactory,
  deps: [SkillsRepository],
};

const getOccupationUseCaseFactory =
  (occupationRepository: OccupationRepository) => new GetOccupationUsecase(occupationRepository);
export const getOccupationUseCaseProvider = {
  provide: GetOccupationUsecase,
  useFactory: getOccupationUseCaseFactory,
  deps: [OccupationRepository],
};

const addPostsUseCaseFactory =
  (postsRepository: PostsRepository) => new AddPostUseCase(postsRepository);
export const addPostsUseCaseProvider = {
  provide: AddPostUseCase,
  useFactory: addPostsUseCaseFactory,
  deps: [PostsRepository],
};

const getPostsUseCaseFactory =
  (postsRepository: PostsRepository) => new GetPostUseCase(postsRepository);
export const getPostsUseCaseProvider = {
  provide: GetPostUseCase,
  useFactory: getPostsUseCaseFactory,
  deps: [PostsRepository],
};

const getPostsByUserUseCaseFactory =
  (postsRepository: PostsRepository) => new GetJobsByUserUsecase(postsRepository);
export const getByUserPostsUseCaseProvider = {
  provide: GetJobsByUserUsecase,
  useFactory: getPostsByUserUseCaseFactory,
  deps: [PostsRepository],
};

const applyUseCaseFactory =
  (postsRepository: PostsRepository) => new ApplyUsecase(postsRepository);
export const applyUseCaseProvider = {
  provide: ApplyUsecase,
  useFactory: applyUseCaseFactory,
  deps: [PostsRepository],
};

const hireUseCaseFactory =
  (postsRepository: PostsRepository) => new HireUsecase(postsRepository);
export const hireUseCaseProvider = {
  provide: HireUsecase,
  useFactory: hireUseCaseFactory,
  deps: [PostsRepository],
};

const myApplicationsUseCaseFactory =
  (postsRepository: PostsRepository) => new MyApplicationsUsecase(postsRepository);
export const myApplicationsUseCaseProvider = {
  provide: MyApplicationsUsecase,
  useFactory: myApplicationsUseCaseFactory,
  deps: [PostsRepository],
};

const rateUseCaseFactory =
  (postsRepository: PostsRepository) => new RateUsecase(postsRepository);
export const rateUseCaseProvider = {
  provide: RateUsecase,
  useFactory: rateUseCaseFactory,
  deps: [PostsRepository],
};


const getPendingConnectionsUseCaseFactory =
  (connectionRepository: ConnectionRepository) => new GetPendingConnectionsUsecase(connectionRepository);
export const getPendingConnectionsUseCaseProvider = {
  provide: GetPendingConnectionsUsecase,
  useFactory: getPendingConnectionsUseCaseFactory,
  deps: [ConnectionRepository],
};

const acceptOrDenyConnectionRequestUseCaseFactory =
  (connectionRepository: ConnectionRepository) => new AcceptordenyConnectionUsecase(connectionRepository);
export const acceptOrDenyConnectionRequestUseCaseProvider = {
  provide: AcceptordenyConnectionUsecase,
  useFactory: acceptOrDenyConnectionRequestUseCaseFactory,
  deps: [ConnectionRepository],
};

const getUserConnectionsUseCaseFactory =
  (connectionRepository: ConnectionRepository) => new GetUserConnectionsUsecase(connectionRepository);
export const getUserConnectionsUseCaseProvider = {
  provide: GetUserConnectionsUsecase,
  useFactory: getUserConnectionsUseCaseFactory,
  deps: [ConnectionRepository],
};
const getSuggestionsUseCaseFactory =
  (connectionRepository: ConnectionRepository) => new GetSuggestionsUsecase(connectionRepository);
export const getSuggestionsUseCaseProvider = {
  provide: GetSuggestionsUsecase,
  useFactory: getSuggestionsUseCaseFactory,
  deps: [ConnectionRepository],
};

const addConnectionUseCaseFactory =
  (connectionRepository: ConnectionRepository) => new AddConnectionUsecase(connectionRepository);
export const addConnectionUseCaseProvider = {
  provide: AddConnectionUsecase,
  useFactory: addConnectionUseCaseFactory,
  deps: [ConnectionRepository],
};

const getPendingJobSpaceInvitationsUseCaseFactory =
  (jobspaceRepository: JobspaceRepository) => new GetPendingInvitationsUsecase(jobspaceRepository);
export const getPendingJobSpaceInvitationsUseCaseProvider = {
  provide: GetPendingInvitationsUsecase,
  useFactory: getPendingJobSpaceInvitationsUseCaseFactory,
  deps: [JobspaceRepository],
};
const createJobSpaceUseCaseFactory =
  (jobspaceRepository: JobspaceRepository) => new CreateJobspaceUsecase(jobspaceRepository);
export const createJobSpaceUseCaseProvider = {
  provide: CreateJobspaceUsecase,
  useFactory: createJobSpaceUseCaseFactory,
  deps: [JobspaceRepository],
};
const addUserToJobSpaceUseCaseFactory =
  (jobspaceRepository: JobspaceRepository) => new AddUserToJobspaceUsecase(jobspaceRepository);
export const addUserToJobSpaceUseCaseProvider = {
  provide: AddUserToJobspaceUsecase,
  useFactory: addUserToJobSpaceUseCaseFactory,
  deps: [JobspaceRepository],
};
const acceptOrDenyInvitationUseCaseFactory =
  (jobspaceRepository: JobspaceRepository) => new AcceptOrDenyJobSpaceInvitationUsecase(jobspaceRepository);
export const acceptOrDenyInvitationUseCaseProvider = {
  provide: AcceptOrDenyJobSpaceInvitationUsecase,
  useFactory: acceptOrDenyInvitationUseCaseFactory,
  deps: [JobspaceRepository],
};

const getUserJobSpacesUseCaseFactory =
  (jobspaceRepository: JobspaceRepository) => new GetUserJobspaces(jobspaceRepository);
export const getUserJobSpacesUseCaseProvider = {
  provide: GetUserJobspaces,
  useFactory: getUserJobSpacesUseCaseFactory,
  deps: [JobspaceRepository],
};

const getNotificationsFactory =
  (notificationRepository: NotificationRepository) => new GetNotificationUsecase(notificationRepository);
export const getNotificationProvider = {
  provide: GetNotificationUsecase,
  useFactory: getNotificationsFactory,
  deps: [NotificationRepository],
};

const getMessagesFactory =
  (chatRepository: ChatRepository) => new GetMessagesUsecase(chatRepository);
export const getMessagesProvider = {
  provide: GetMessagesUsecase,
  useFactory: getMessagesFactory,
  deps: [ChatRepository],
};

const getChatRoomFactory =
  (chatRepository: ChatRepository) => new GetChatRoomUsecase(chatRepository);
export const getChatRoomProvider = {
  provide: GetChatRoomUsecase,
  useFactory: getChatRoomFactory,
  deps: [ChatRepository],
};

const getChatRoomsFactory =
  (chatRepository: ChatRepository) => new GetChatRoomsUsecase(chatRepository);
export const getChatRoomsProvider = {
  provide: GetChatRoomsUsecase,
  useFactory: getChatRoomsFactory,
  deps: [ChatRepository],
};

const sendMessageFactory =
  (chatRepository: ChatRepository) => new SendMessageUsecase(chatRepository);
export const sendMessageProvider = {
  provide: SendMessageUsecase,
  useFactory: sendMessageFactory,
  deps: [ChatRepository],
};

@NgModule({
  providers: [
    userLoginUseCaseProvider,
    userRegisterUseCaseProvider,
    {provide: UserRepository, useClass: UserImplementationRepository},
    getUserProfileUseCaseProvider,
    addUserProfileUseCaseProvider,
    updateUserProfileImageUseCaseProvider,
    updateUserSignatureUseCaseProvider,
    searchUseCaseProvider,
    {provide: UserProfileRepository, useClass: UserProfileImplementationRepository},
    getSkillsUseCaseProvider,
    {provide: SkillsRepository, useClass: SkillsImplementationRepository},
    getOccupationUseCaseProvider,
    {provide: OccupationRepository, useClass: OccupationImplementationRepository},
    addPostsUseCaseProvider,
    getPostsUseCaseProvider,
    applyUseCaseProvider,
    hireUseCaseProvider,
    getByUserPostsUseCaseProvider,
    myApplicationsUseCaseProvider,
    rateUseCaseProvider,
    {provide: PostsRepository, useClass: PostsImplementationRepository},
    getPendingConnectionsUseCaseProvider,
    getSuggestionsUseCaseProvider,
    acceptOrDenyConnectionRequestUseCaseProvider,
    getUserConnectionsUseCaseProvider,
    addConnectionUseCaseProvider,
    {provide: ConnectionRepository, useClass: ConnectionImplementationRepository},
    getPendingJobSpaceInvitationsUseCaseProvider,
    createJobSpaceUseCaseProvider,
    addUserToJobSpaceUseCaseProvider,
    acceptOrDenyInvitationUseCaseProvider,
    getUserJobSpacesUseCaseProvider,
    {provide: JobspaceRepository, useClass: JobspaceImplementationRepository},
    getNotificationProvider,
    {provide: NotificationRepository, useClass: NotificationImplementation},
    getChatRoomProvider,
    getMessagesProvider,
    sendMessageProvider,
    getChatRoomsProvider,
    {provide: ChatRepository, useClass: ChatImplementationRepository},

  ],
  imports: [
    CommonModule,
    HttpClientModule,
  ],
})
export class DataModule {
}
