export interface UserEntity {
  id: string;
  email: string;
  userName: string;
  token: string;
}
