import {Observable, of} from "rxjs";
import {catchError, map} from "rxjs/operators";
import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {JobspaceRepository} from "./jobspace.repository";
import {JobspaceRepositoryMapper} from "./jobspace-repository.mapper";
import {JobSpaceModel} from "../../model/job-space.model";
import {UserProfileModel} from "../../model/user-profile.model";


@Injectable({
  providedIn: 'root',
})
export class JobspaceImplementationRepository extends JobspaceRepository {

  jobspaceRepositoryMapper = new JobspaceRepositoryMapper();

  constructor(private http: HttpClient) {
    super();
  }

  override getPendingJobSpacesInvitations(): Observable<JobSpaceModel[]> {
    return this.http.get<JobSpaceModel[]>('http://localhost:8091/api/jobSpace/getUserPendingJobSpaceInvitations').pipe(
      map((response: JobSpaceModel[]) => {
        return response.map(this.jobspaceRepositoryMapper.mapFrom);
      }),
      catchError(error => {
        // Forward the error downstream
        return of([]);
      })
    );
  }

  acceptOrDenyInvitation(param: {
    jobSpaceID: string;
    userProfileID: string;
    userDecision: boolean
  }): Observable<boolean> {
    const params = new HttpParams()
      .set('jobSpaceID', param.jobSpaceID)
      .set('userProfileID', param.userProfileID)
      .set('userDecision', param.userDecision);
    return this.http.post<boolean>('http://localhost:8091/api/jobSpace/acceptOrDenyInvitation', {}, {params}).pipe(
      catchError(error => {
        return of(false);
      })
    );
  }

  addUserToJobSpace(param: { jobSpaceID: string; userProfile: UserProfileModel }): Observable<JobSpaceModel> {
    const params = new HttpParams()
      .set('jobSpaceID', param.jobSpaceID)

    return this.http.post<JobSpaceModel>('http://localhost:8091/api/jobSpace/addUserToJobSpace', param.userProfile, {params}).pipe(
      map(this.jobspaceRepositoryMapper.mapFrom),
    );
  }

  createJobSpace(param: { jobSpace: JobSpaceModel }): Observable<JobSpaceModel> {
    return this.http.put<JobSpaceModel>('http://localhost:8091/api/jobSpace', param.jobSpace,).pipe(
      map(this.jobspaceRepositoryMapper.mapFrom),
    );
  }

  getUserJobSpaces(): Observable<JobSpaceModel[]> {
    return this.http.get<JobSpaceModel[]>('http://localhost:8091/api/jobSpace/getUserJobSpaces').pipe(
      map((response: JobSpaceModel[]) => {
        return response.map(this.jobspaceRepositoryMapper.mapFrom);
      }),
      catchError(error => {
        return of([]);
      })
    );
  }
}
