import {Observable} from "rxjs";
import {JobSpaceModel} from "../../model/job-space.model";
import {UserProfileModel} from "../../model/user-profile.model";

export abstract class JobspaceRepository {
  abstract getPendingJobSpacesInvitations(): Observable<JobSpaceModel[]>;

  abstract createJobSpace(param: { jobSpace: JobSpaceModel }): Observable<JobSpaceModel>;

  abstract addUserToJobSpace(param: { jobSpaceID: string, userProfile: UserProfileModel }): Observable<JobSpaceModel>;

  abstract acceptOrDenyInvitation(param: {
    jobSpaceID: string,
    userProfileID: string,
    userDecision: boolean
  }): Observable<boolean>;

  abstract getUserJobSpaces(): Observable<JobSpaceModel[]>;

}
