import {Mapper} from "../../../base/utils/mapper";
import {JobSpaceModel} from "../../model/job-space.model";

export class JobspaceRepositoryMapper extends Mapper<JobSpaceModel, JobSpaceModel> {
  mapFrom(param: JobSpaceModel): JobSpaceModel {
    return {
      id: param.id,
      name: param.name,
      admin: param.admin,
      staff: param.staff
    };
  }

  mapTo(param: JobSpaceModel): JobSpaceModel {
    return {
      id: param.id,
      name: param.name,
      admin: param.admin,
      staff: param.staff
    };
  }
}
