import {Observable} from 'rxjs';
import {UserModel} from "../../model/user.model";
import {TokenModel} from "../../model/token.model";

export abstract class UserRepository {
  abstract login(params: { username: string, password: string }): Observable<TokenModel>;

  abstract register(params: {
    email: string,
    password: string,
    username: string,

  }): Observable<UserModel>;


  abstract logout(): Observable<any>;

}
