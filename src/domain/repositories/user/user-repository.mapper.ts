import {Mapper} from "../../../base/utils/mapper";
import {UserEntity} from "../../data/entities/user-entity";
import {UserModel} from "../../model/user.model";

export class UserRepositoryMapper extends Mapper<UserEntity, UserModel> {
  mapFrom(param: UserEntity): UserModel {
    return {
      id: param.id,
      email: param.email,
      username: param.userName,
      token: param.token
    };
  }

  mapTo(param: UserModel): UserEntity {
    return {
      id: param.id,
      userName: param.username,
      email: param.email,
      token: param.token

    }
  }
}
