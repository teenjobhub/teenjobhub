import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserRepository} from "./user.repository";
import {UserRepositoryMapper} from "./user-repository.mapper";
import {UserModel} from "../../model/user.model";
import {UserEntity} from "../../data/entities/user-entity";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {TokenModel} from "../../model/token.model";
import {LoginRepositoryMapper} from "./login.repository.mapper";

import {LocalStorageService} from "../../../utils/localstorage";

@Injectable({
  providedIn: 'root',
})
export class UserImplementationRepository extends UserRepository {
  userMapper = new UserRepositoryMapper();
  loginMapper = new LoginRepositoryMapper()

  constructor(private http: HttpClient,private localStorage:LocalStorageService) {
    super();
  }

  login(params: { username: string, password: string }): Observable<TokenModel> {
    const body = new URLSearchParams();
    body.set('grant_type', 'password');
    body.set('username', params.username);
    body.set('password', params.password);

    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Basic ' + btoa('oidc-client:123456789')
    });
    return this.http.post<TokenModel>('http://localhost:8090/oauth2/token', body.toString(), {headers})
      .pipe(map(this.loginMapper.mapFrom))
  }

  register(params: {
    email: string,
    password: string,
    username: string,
  }): Observable<UserModel> {
    return this.http
      .post<UserEntity>('http://localhost:8090/api/auth/signup', params)
      .pipe(map(this.userMapper.mapFrom));
  }

  logout(): Observable<any> {
    return  this.http
      .post<any>('http://localhost:8090/oauth2/revoke', {"token":this.localStorage.getData('token')});
  }
}
