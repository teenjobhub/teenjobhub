import {Mapper} from "../../../base/utils/mapper";
import {TokenModel} from "../../model/token.model";

export class LoginRepositoryMapper extends Mapper<TokenModel, TokenModel> {
  mapFrom(param: TokenModel): TokenModel {
    return {
      access_token: param.access_token,
      refresh_token: param.refresh_token,
      // Add other properties as needed
    };
  }

  mapTo(param: TokenModel): TokenModel {
    return {
      access_token: param.access_token,
      refresh_token: param.refresh_token,
      // Add other properties as needed
    };
  }
}
