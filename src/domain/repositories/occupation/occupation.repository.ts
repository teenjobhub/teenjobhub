import {Observable} from "rxjs";
import {OccupationModel} from "../../model/occupation.model";

export abstract class OccupationRepository {
  abstract getOccupations(): Observable<OccupationModel[]>;

}
