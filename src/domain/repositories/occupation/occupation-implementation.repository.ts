import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {OccupationRepository} from "./occupation.repository";
import {OccupationModel} from "src/domain/model/occupation.model";
import {OccupationRepositoryMapper} from "./occupation-repository.mapper";

@Injectable({
  providedIn: 'root',
})
export class OccupationImplementationRepository extends OccupationRepository {

  occupationMapper = new OccupationRepositoryMapper();

  constructor(private http: HttpClient) {
    super();
  }

  override getOccupations(): Observable<OccupationModel[]> {
    return this.http.get<OccupationModel[]>('http://localhost:8091/api/occupation').pipe(
      map((response: OccupationModel[]) => response.map(this.occupationMapper.mapFrom)));

  }
}
