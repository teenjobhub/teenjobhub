import {Mapper} from "../../../base/utils/mapper";
import {OccupationModel} from "../../model/occupation.model";

export class OccupationRepositoryMapper extends Mapper<OccupationModel, OccupationModel> {
  mapFrom(param: OccupationModel): OccupationModel {
    return {
      id: param.id,
      name: param.name,
      // Add other properties as needed
    };
  }

  mapTo(param: OccupationModel): OccupationModel {
    return {
      id: param.id,
      name: param.name,
    };
  }
}
