import {Observable} from "rxjs";
import {ConnectionRequestModel} from "../../model/connection-request.model";
import {UserProfileModel} from "../../model/user-profile.model";

export abstract class ConnectionRepository {
  abstract getPendingConnections(): Observable<ConnectionRequestModel[]>;

  abstract acceptOrDenyConnectionRequest(param: {
    userDecision: boolean,
    connectionRequestID: string
  }): Observable<boolean>;

  abstract getUserConnections(): Observable<ConnectionRequestModel[]>;

  abstract addConnection(param: { destinationUserID: string }): Observable<boolean>;

  abstract getSuggestions(): Observable<UserProfileModel[]>;


}
