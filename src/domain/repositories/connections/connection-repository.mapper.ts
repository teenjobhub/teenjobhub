import {Mapper} from "../../../base/utils/mapper";
import {ConnectionRequestModel} from "../../model/connection-request.model";

export class ConnectionRepositoryMapper extends Mapper<ConnectionRequestModel, ConnectionRequestModel> {
  mapFrom(param: ConnectionRequestModel): ConnectionRequestModel {
    return {
      destinationUser: param.destinationUser,
      id: param.id,
      originUser: param.originUser,
      pending: param.pending

    };
  }

  mapTo(param: ConnectionRequestModel): ConnectionRequestModel {
    return {
      destinationUser: param.destinationUser,
      id: param.id,
      originUser: {
        id: param.originUser.id,
        firstName: param.originUser.firstName,
        lastName: param.originUser.lastName,
        phoneNumber: param.originUser.phoneNumber,
        country: param.originUser.country,
        governance: param.originUser.governance,
        userImage: param.originUser.userImage,
        user: {
          id: param.originUser.user.id,
          username: param.originUser.user.username,
          email: param.originUser.user.email,
          token: param.originUser.user.token
        },
        occupation: {
          id: param.originUser.occupation.id,
          name: param.originUser.occupation.name
        },
        skills: []
      }, pending: param.pending

    };
  }
}
