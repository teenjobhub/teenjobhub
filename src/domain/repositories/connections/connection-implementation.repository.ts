import {Observable, of} from "rxjs";
import {catchError, map} from "rxjs/operators";
import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {ConnectionRepository} from "./connection.repository";
import {ConnectionRepositoryMapper} from "./connection-repository.mapper";
import {ConnectionRequestModel} from "../../model/connection-request.model";
import {UserProfileModel} from "src/domain/model/user-profile.model";
import {LocalStorageService} from "../../../utils/localstorage";
import {SkillModel} from "../../model/skill.model";


@Injectable({
    providedIn: 'root',
})
export class ConnectionImplementationRepository extends ConnectionRepository {

    connectionMapper = new ConnectionRepositoryMapper();


    constructor(private http: HttpClient,private localStorage:LocalStorageService) {
        super();
    }

    override getPendingConnections(): Observable<ConnectionRequestModel[]> {
        return this.http.get<{content:ConnectionRequestModel[]}>('http://localhost:8091/api/network/getUserReceivedRequests').pipe(
            map((response: { content: ConnectionRequestModel[] }) => response.content.map(this.connectionMapper.mapFrom)),
            catchError(error => {
                return of([]);
            })
        );
    }

    acceptOrDenyConnectionRequest(param: { userDecision: boolean, connectionRequestID: string }): Observable<boolean> {
        const params = new HttpParams()
            .set('userDecision', param.userDecision)
            .set('connectionID', param.connectionRequestID);
        return this.http.post<boolean>('http://localhost:8091/api/network', {}, {params}).pipe(
            catchError(error => {
                return of(false);
            })
        );
    }

    getUserConnections(): Observable<ConnectionRequestModel[]> {
        return this.http.get<{content:ConnectionRequestModel[]}>('http://localhost:8091/api/network/getUserConnections').pipe(
          map((response: { content: ConnectionRequestModel[] }) => response.content.map(this.connectionMapper.mapFrom)),
      catchError(error => {
                return of([]);
            })
        );
    }

    addConnection(param: { destinationUserID: string }): Observable<boolean> {
        const params = new HttpParams()
            .set('destinationUserID', param.destinationUserID)
        return this.http.put<boolean>('http://localhost:8091/api/network', {}, {params}).pipe(
            map(response=>response),
        );
    }


    override getSuggestions(): Observable<UserProfileModel[]> {
        return this.http.get<UserProfileModel[]>('http://localhost:8091/api/network/suggestions',).pipe(
            map((response: UserProfileModel[]) => response),
            catchError(error => {
                return of([]);
            }));
    }

}
