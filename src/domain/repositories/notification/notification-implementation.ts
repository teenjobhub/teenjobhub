import {NotificationRepository} from "./notification.repository";
import {Observable, of} from "rxjs";
import {NotificationModel} from "../../model/notifications.model";
import {catchError, map} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
import {NotificationRepositoryMapper} from "./notification-repository.mapper";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root',
})
export class NotificationImplementation implements NotificationRepository {

  notificationRepositoryMapper: NotificationRepositoryMapper = new NotificationRepositoryMapper()

  constructor(private http: HttpClient) {
  }

  getNotificationsByUser(): Observable<NotificationModel[]> {
    return this.http.get<NotificationModel[]>('http://localhost:8091/api/notification/findByUserID').pipe(
      map((response: NotificationModel[]) => response.map(this.notificationRepositoryMapper.mapFrom)),
      catchError(error => {
        return of([]);
      })
    );
  }

}
