import {Mapper} from "../../../base/utils/mapper";
import {NotificationModel} from "../../model/notifications.model";

export class NotificationRepositoryMapper extends Mapper<any, NotificationModel> {
  mapFrom(param: NotificationModel): NotificationModel {
    return {
      context: param.context,
      id: param.id,
      message: param.message,
      receiver: param.receiver,
      seen: param.seen,
      sender: param.sender
    };
  }

  mapTo(param: NotificationModel): NotificationModel {
    return {
      context: param.context,
      id: param.id,
      message: param.message,
      receiver: param.receiver,
      seen: param.seen,
      sender: param.sender
    };
  }
}

