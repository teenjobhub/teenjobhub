import {Observable} from "rxjs";
import {NotificationModel} from "../../model/notifications.model";

export abstract class NotificationRepository {
  abstract getNotificationsByUser(): Observable<NotificationModel[]>;


}
