import {Observable} from "rxjs";
import {UserProfileModel} from "../../model/user-profile.model";

export abstract class UserProfileRepository {
  abstract getUserProfile(userID: string): Observable<UserProfileModel>;

  abstract addUserProfile(userProfileModel: UserProfileModel, userImage: File): Observable<UserProfileModel>;

  abstract updateUserProfileImage(userImage: File, id: string): Observable<boolean>;

  abstract updateUserSignature(userImage: File, id: string): Observable<UserProfileModel>;

  abstract search(query: string): Observable<UserProfileModel[]>;

  abstract findById(query: string): Observable<UserProfileModel>;

}
