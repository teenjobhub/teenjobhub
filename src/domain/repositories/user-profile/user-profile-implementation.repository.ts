import {UserProfileRepository} from "./user-profile.repository";
import {Observable} from "rxjs";
import {UserProfileModel} from "../../model/user-profile.model";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {map} from "rxjs/operators";
import {UserProfileRepositoryMapper} from "./user-profile.repository.mapper";


@Injectable({
  providedIn: 'root',
})
export class UserProfileImplementationRepository extends UserProfileRepository {

  constructor(private http: HttpClient) {
    super();

  }

  userProfileMapper: UserProfileRepositoryMapper = new UserProfileRepositoryMapper();

  addUserProfile(userProfileModel: UserProfileModel): Observable<UserProfileModel> {

    return this.http.put<UserProfileModel>("http://localhost:8091/api/user", userProfileModel).pipe();
  }

  getUserProfile(): Observable<UserProfileModel> {
    return this.http.get<UserProfileModel>(`http://localhost:8091/api/user`, {}).pipe();
  }

  updateUserProfileImage(userImage: File, id: string): Observable<boolean> {
    const formData = new FormData();
    formData.append('userImage', new Blob([userImage], {type: 'text/plain'},), userImage.name)
    formData.append('id', id)
    return this.http.post<boolean>("http://localhost:8091/api/user", formData).pipe();
  }

  search(query: string): Observable<UserProfileModel[]> {
    const params = new HttpParams()
      .set('query', query)
    return this.http.get<{ content: UserProfileModel[] }>('http://localhost:8091/api/user/search', {params}).pipe(
      map((response: { content: UserProfileModel[] }) => response.content.map(this.userProfileMapper.mapFrom)),);
  }

  findById(userProfileId: string): Observable<UserProfileModel> {
    const params = new HttpParams()
      .set('userProfileId', userProfileId)
    return this.http.get<UserProfileModel>('http://localhost:8091/api/user/findById', {params}).pipe(
      map((response => response)))

  }

  updateUserSignature(userImage: File, id: string): Observable<UserProfileModel> {
    const formData = new FormData();
    formData.append('userSignature', new Blob([userImage], {type: 'text/plain'},), userImage.name)
    formData.append('id', id)
    return this.http.post<UserProfileModel>("http://localhost:8091/api/user/updateSignature", formData).pipe(map((response=>response)));
  }
}
