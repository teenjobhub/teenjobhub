import {Mapper} from "../../../base/utils/mapper";
import {SkillModel} from "../../model/skill.model";
import {UserProfileModel} from "../../model/user-profile.model";


export class UserProfileRepositoryMapper extends Mapper<UserProfileModel, UserProfileModel> {
    mapFrom(param: UserProfileModel): UserProfileModel {
        return {
            country: param.country,
            firstName: param.firstName,
            governance: param.governance,
            id: param.id,
            lastName: param.lastName,
            occupation: {
                id: "",
                name: ""
            },
            phoneNumber: param.phoneNumber,
            skills: [],
            user: {
                id: "",
                username: "",
                email: "",
                token: ""
            },
            userImage: ""

            // Add other properties as needed
        };
    }

    mapTo(param: UserProfileModel): UserProfileModel {
        return {
            country: param.country,
            firstName: param.firstName,
            governance: param.governance,
            id: param.id,
            lastName: param.lastName,
            occupation: {
                id: "",
                name: ""
            },
            phoneNumber: param.phoneNumber,
            skills: [],
            user: {
                id: "",
                username: "",
                email: "",
                token: ""
            },
            userImage: ""

            // Add other properties as needed
        };
    }
}

