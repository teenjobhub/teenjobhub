import {Observable} from "rxjs";
import {SkillModel} from "../../model/skill.model";

export abstract class SkillsRepository {
  abstract getSkills(): Observable<SkillModel[]>;

}
