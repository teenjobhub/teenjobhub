import {SkillsRepository} from "./skills.repository";
import {Observable} from "rxjs";
import {SkillModel} from "../../model/skill.model";
import {map} from "rxjs/operators";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {SkillsImplementationRepositoryMapper} from "./skills-repository.mapper";

@Injectable({
  providedIn: 'root',
})
export class SkillsImplementationRepository extends SkillsRepository {
  skillsMapper = new SkillsImplementationRepositoryMapper();

  constructor(private http: HttpClient) {
    super();
  }

  getSkills(): Observable<SkillModel[]> {
    return this.http.get<{ content: SkillModel[] }>('http://localhost:8091/api/skill').pipe(
      map((response: { content: SkillModel[] }) => response.content.map(this.skillsMapper.mapFrom)));
  }
}
