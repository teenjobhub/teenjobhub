import {Mapper} from "../../../base/utils/mapper";
import {SkillModel} from "../../model/skill.model";


export class SkillsImplementationRepositoryMapper extends Mapper<SkillModel, SkillModel> {
  mapFrom(param: SkillModel): SkillModel {
    return {
      id: param.id,
      name: param.name,
      // Add other properties as needed
    };
  }

  mapTo(param: SkillModel): SkillModel {
    return {
      id: param.id,
      name: param.name,
    };
  }
}

