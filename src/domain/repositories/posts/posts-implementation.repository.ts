import {PostsRepository} from "./posts.repository";
import {Observable, of, tap} from "rxjs";
import {catchError, map} from "rxjs/operators";
import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {AddPostModel} from "../../model/add-post.model";
import {JobPostModel} from "../../model/job-post.model";
import {JobsRepositoryMapper} from "./jobs-repository.mapper";
import {MessageModel} from "../../model/message";



@Injectable({
  providedIn: 'root',
})
export class PostsImplementationRepository extends PostsRepository {
  jobsRepositoryMapper = new JobsRepositoryMapper();

  constructor(private http: HttpClient) {
    super();
  }

  getPosts(): Observable<JobPostModel[]> {
    return this.http.get<JobPostModel[]>('http://localhost:8092/api/jobs').pipe(
      map((response: JobPostModel[]) => response));
  }

  addPost(params: { postsModel: AddPostModel }): Observable<JobPostModel> {
    const formData = new FormData();
    formData.append('jobPostTitle', params.postsModel.jobPostTitle);

    formData.append('jobPostDescription', params.postsModel.jobPostDescription);
    formData.append('image', new Blob([params.postsModel.postImage], {type: 'text/plain'},), params.postsModel.postImage.name); // assuming postImage is a File or Blob

    return this.http.put<JobPostModel>('http://localhost:8092/api/jobs', formData).pipe(
      map(this.jobsRepositoryMapper.mapFrom)
    );
  }

  apply(jobPostID: string): Observable<boolean> {
    const params = new HttpParams()
      .set('jobPostID', jobPostID)

    return this.http.post<boolean>('http://localhost:8092/api/jobs/apply', {}, {params}).pipe(
      catchError(error => {
        return of(false);
      })
    );
  }

  findByUserID(closed:boolean): Observable<JobPostModel[]> {
    const params = new HttpParams()
      .set('mine', true)
      .set('closed',closed)
    return this.http.get<JobPostModel[]>('http://localhost:8092/api/jobs', {params}).pipe(
      map((response: JobPostModel[]) => response));
  }

  myApplications(): Observable<JobPostModel[]> {
    return this.http.get<JobPostModel[]>('http://localhost:8092/api/jobs/myApplications', ).pipe(
      map((response: JobPostModel[]) => response));
  }

  hire(jobPostID: string, userProfileID: string): Observable<MessageModel> {
    const params = new HttpParams()
      .set('jobPostID', jobPostID)
      .set('userProfileID', userProfileID)
    return this.http.post<MessageModel>('http://localhost:8092/api/jobs/hire', {}, {params}).pipe(
      tap((response: MessageModel) => console.log(`http://localhost:8092/file${response.message}`)),
      map((response: MessageModel) => response),

    );
  }

  rate(jobPostID: string, rate: number): Observable<boolean> {
     const params = new HttpParams()
      .set('jobPostID', jobPostID)
      .set('rating', rate)
    return this.http.post<boolean>('http://localhost:8092/api/jobs/rate', {}, {params}).pipe(
      map((response: boolean) => response),

    );
  }
}
