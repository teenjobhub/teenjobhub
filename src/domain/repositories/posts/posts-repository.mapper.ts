import {Mapper} from "../../../base/utils/mapper";
import {AddPostModel} from "../../model/add-post.model";


export class PostsRepositoryMapper extends Mapper<AddPostModel, AddPostModel> {
  mapFrom(param: AddPostModel): AddPostModel {
    return {
      jobPostTitle:param.jobPostTitle,
      postImage: param.postImage,
      jobPostDescription: param.jobPostDescription,

    };
  }

  mapTo(param: AddPostModel): AddPostModel {
    return {
      jobPostTitle:param.jobPostTitle,

      postImage: param.postImage,
      jobPostDescription: param.jobPostDescription
    };
  }
}

