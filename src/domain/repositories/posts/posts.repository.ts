import {Observable} from "rxjs";
import {AddPostModel} from "../../model/add-post.model";
import {JobPostModel} from "../../model/job-post.model";
import {MessageModel} from "../../model/message";

export abstract class PostsRepository {
  abstract getPosts(): Observable<JobPostModel[]>;

  abstract findByUserID(closed:boolean): Observable<JobPostModel[]>;

  abstract addPost(params: { postsModel: AddPostModel }): Observable<JobPostModel>;

  abstract apply(jobPostID: string): Observable<boolean>;

  abstract myApplications(): Observable<JobPostModel[]>;

  abstract rate(jobPostID: string,rate:number): Observable<boolean>;

  abstract hire(jobPostID: string, userProfileID: string): Observable<MessageModel>;
}
