import {Mapper} from "../../../base/utils/mapper";
import {JobPostModel} from "../../model/job-post.model";

export class JobsRepositoryMapper extends Mapper<JobPostModel, JobPostModel> {
  mapFrom(param: JobPostModel): JobPostModel {
    return {
      jobPostTitle:param.jobPostTitle,
      applicants: param.applicants,
      hiredUser: {
        id: param.hiredUser.id,
        firstName: param.hiredUser.firstName,
        lastName: param.hiredUser.lastName,
        phoneNumber: param.hiredUser.phoneNumber,
        country: param.hiredUser.country,
        governance: param.hiredUser.governance,
        userImage: param.hiredUser.userImage,
        user: {
          id: param.hiredUser.user.id,
          username: param.hiredUser.user.username,
          email: param.hiredUser.user.email,
          token: ""
        },
        occupation: {
          id: param.hiredUser.occupation.id,
          name: param.hiredUser.occupation.name
        },
        skills: []
      }, open: false,
      comments: param.comments,
      id: param.id,
      jobPostDescription: param.jobPostDescription,
      jobSpace: param.jobSpace,
      postDate: param.postDate,
      postImageUrl: `http://localhost:8092/file/${param.postImageUrl}`,
      user: param.user
    };
  }

  mapTo(param: JobPostModel): JobPostModel {
    return {
      jobPostTitle:param.jobPostTitle,

      applicants: [], hiredUser: {
        id: "",
        firstName: "",
        lastName: "",
        phoneNumber: "",
        country: "",
        governance: "",
        userImage: "",
        user: {
          id: "",
          username: "",
          email: "",
          token: ""
        },
        occupation: {
          id: "",
          name: ""
        },
        skills: []
      }, open: false,
      comments: param.comments,
      id: param.id,
      jobPostDescription: param.jobPostDescription,
      jobSpace: param.jobSpace,
      postDate: param.postDate,
      postImageUrl: param.postImageUrl,
      user: param.user

    };
  }
}
