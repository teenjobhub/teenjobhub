import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {ChatRepository} from "./chat.repository";
import {Observable, of} from "rxjs";
import {ChatRoomModel} from "../../model/chat-room.model";
import {ChatMessageModel} from "../../model/chat-message.model";
import {catchError, map} from "rxjs/operators";
import {LocalStorageService} from "../../../utils/localstorage";


@Injectable({
  providedIn: 'root',
})
export class ChatImplementationRepository extends ChatRepository {
  constructor(private http: HttpClient,private localStorage:LocalStorageService) {
    super();
  }

  getChatRoom(chatRoomOrUserProfileId: string): Observable<ChatRoomModel> {
    return this.http.get<ChatRoomModel>(`http://localhost:8091/api/chat/${chatRoomOrUserProfileId}`).pipe(
      map((response) => response),
      catchError(error => {
        return of();
      })
    );
  }

  getMessages(chatRoomId: string): Observable<ChatMessageModel[]> {
    const params = new HttpParams()
      .set('chatRoomId', chatRoomId)
    return this.http.get<{ content: ChatMessageModel[] }>(`http://localhost:8091/api/chat/messages`, {params}).pipe(
      map((response) => response.content),
      catchError(error => {
        return of();
      }));
  }

  sendMessage(chatRoomId: string, message: string): Observable<boolean> {
    const params = new HttpParams()
      .set('chatRoomId', chatRoomId)
    return this.http.put<boolean>(`http://localhost:8091/api/chat`, message, {params}).pipe(
      map((response) => response),
      catchError(error => {
        return of();
      })
    );
  }

  getChatRooms(userProfileId: string): Observable<ChatRoomModel[]> {
    console.log(userProfileId)
    const params = new HttpParams()
      .set('userProfileId', this.localStorage.getData('userProfile').id)
    return this.http.get<{ content: ChatRoomModel[] }>(`http://localhost:8091/api/chat`, {params}).pipe(
      map((response) => response.content),
      catchError(error => {
        return of();
      }));  }


}
