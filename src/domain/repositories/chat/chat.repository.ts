import {Observable} from "rxjs";
import {ConnectionRequestModel} from "../../model/connection-request.model";
import {UserProfileModel} from "../../model/user-profile.model";
import {ChatMessageModel} from "../../model/chat-message.model";
import {ChatRoomModel} from "../../model/chat-room.model";

export abstract class ChatRepository {

  abstract sendMessage(chatRoomId:string,message:string): Observable<boolean>;

  abstract getChatRoom(chatRoomOrUserProfileId:string): Observable<ChatRoomModel>;

  abstract getChatRooms(userProfileId:string): Observable<ChatRoomModel[]>;

  abstract getMessages(chatRoomId:string): Observable<ChatMessageModel[]>;

}
