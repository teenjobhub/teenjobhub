import {UserProfileModel} from "./user-profile.model";
import {ChatRoomModel} from "./chat-room.model";

export interface ChatMessageModel {
  id:string,
  body:string,
  sender:UserProfileModel,
  chatRoom:ChatRoomModel
}
