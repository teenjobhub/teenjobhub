import {UserModel} from "./user.model";
import {OccupationModel} from "./occupation.model";
import {SkillModel} from "./skill.model";

export interface UserProfileModel {
  id: string
  firstName: string
  lastName: string
  phoneNumber: string
  country: string
  governance: string
  userImage: string
  user: UserModel
  occupation: OccupationModel
  skills: SkillModel[],

}
