export interface AddPostModel {
  jobPostTitle: string;
  jobPostDescription: string;
  postImage: File;
}
