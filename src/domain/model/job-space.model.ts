import {UserProfileModel} from "./user-profile.model";

export interface JobSpaceModel {
  name: string,
  id: string,
  admin: UserProfileModel,
  staff: Staff[]
}

export interface Staff {
  userProfile: UserProfileModel,
  pending: boolean
}
