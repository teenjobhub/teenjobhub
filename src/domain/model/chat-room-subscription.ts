import {UserProfileModel} from "./user-profile.model";
import {ChatMessageModel} from "./chat-message.model";

export interface ChatRoomSubscription {
  userProfile:UserProfileModel,
  lastSeenMessage:ChatMessageModel
}
