import {UserProfileModel} from "./user-profile.model";

export interface JobPostModel {
  id: string
  jobPostTitle: string
  jobPostDescription: string
  postDate: string
  postImageUrl: string
  user: UserProfileModel
  comments: Comment[]
  jobSpace: any,
  applicants: UserProfileModel[],
  open: boolean,
  hiredUser: UserProfileModel
}
