import {UserProfileModel} from "./user-profile.model";

export interface NotificationModel {

  id: string,
  message: string,
  seen: boolean,
  receiver: UserProfileModel,
  sender: UserProfileModel,
  context: NotificationContext,
}

export enum NotificationContext {
  CONNEXION="CONNEXION",
  JOBSPACE="JOBSPACE",
  JOBPOST="JOBPOST"
}
