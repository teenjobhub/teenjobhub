import {UserProfileModel} from "./user-profile.model";

export interface ConnectionRequestModel {
  id: string
  pending: boolean
  originUser: UserProfileModel
  destinationUser: UserProfileModel
}
