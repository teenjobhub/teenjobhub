import {ChatRoomSubscription} from "./chat-room-subscription";
import {ChatMessageModel} from "./chat-message.model";

export interface ChatRoomModel{
  id:string,
  members:ChatRoomSubscription[],
  last:ChatMessageModel,
  unread:boolean
}
