import {UseCase} from "../../base/use-case";
import {Observable} from "rxjs";
import {SkillModel} from "../../model/skill.model";
import {SkillsRepository} from "../../repositories/skills/skills.repository";

export class GetSkillsUsecase implements UseCase<void, SkillModel[]> {
  constructor(private skillRepository: SkillsRepository) {
  }

  execute(): Observable<SkillModel[]> {
    return this.skillRepository.getSkills();
  }
}
