import {UseCase} from "../../base/use-case";
import {JobspaceRepository} from "../../repositories/jobspace/jobspace.repository";
import {Observable} from "rxjs";
import {JobSpaceModel} from "../../model/job-space.model";

export class CreateJobspaceUsecase implements UseCase<any, JobSpaceModel> {
  constructor(private jobspaceRepository: JobspaceRepository) {
  }

  execute(param: { jobSpace: JobSpaceModel }): Observable<JobSpaceModel> {
    return this.jobspaceRepository.createJobSpace(param);
  }
}
