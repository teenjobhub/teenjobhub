import {UseCase} from "../../base/use-case";
import {Observable} from "rxjs";
import {JobSpaceModel} from "../../model/job-space.model";
import {JobspaceRepository} from "../../repositories/jobspace/jobspace.repository";

export class GetPendingInvitationsUsecase implements UseCase<void, JobSpaceModel[]> {
  constructor(private jobspaceRepository: JobspaceRepository) {
  }

  execute(): Observable<JobSpaceModel[]> {
    return this.jobspaceRepository.getPendingJobSpacesInvitations();
  }
}
