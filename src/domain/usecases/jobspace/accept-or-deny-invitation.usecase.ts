import {UseCase} from "../../base/use-case";
import {Observable} from "rxjs";
import {JobspaceRepository} from "../../repositories/jobspace/jobspace.repository";

export class AcceptOrDenyJobSpaceInvitationUsecase implements UseCase<any, boolean> {
  constructor(private jobspaceRepository: JobspaceRepository) {
  }

  execute(param: { jobSpaceID: string, userProfileID: string, userDecision: boolean }): Observable<boolean> {
    return this.jobspaceRepository.acceptOrDenyInvitation(param);
  }
}
