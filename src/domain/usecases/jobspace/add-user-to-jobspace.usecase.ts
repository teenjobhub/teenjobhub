import {UseCase} from "../../base/use-case";

import {Observable} from "rxjs";
import {JobSpaceModel} from "../../model/job-space.model";
import {JobspaceRepository} from "../../repositories/jobspace/jobspace.repository";
import {UserProfileModel} from "../../model/user-profile.model";

export class AddUserToJobspaceUsecase implements UseCase<any, JobSpaceModel> {
  constructor(private jobspaceRepository: JobspaceRepository) {
  }

  execute(param: { jobSpaceID: string, userProfile: UserProfileModel }): Observable<JobSpaceModel> {
    return this.jobspaceRepository.addUserToJobSpace(param);
  }
}
