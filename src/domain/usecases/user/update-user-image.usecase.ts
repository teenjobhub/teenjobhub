import {UseCase} from "../../base/use-case";
import {UserProfileRepository} from "../../repositories/user-profile/user-profile.repository";
import {Observable} from "rxjs";

export class UpdateUserImageUseCase implements UseCase<any, boolean> {
  constructor(private userRepository: UserProfileRepository) {
  }

  execute(params: { userImage: File, id: string }): Observable<boolean> {
    return this.userRepository.updateUserProfileImage(params.userImage, params.id);
  }
}
