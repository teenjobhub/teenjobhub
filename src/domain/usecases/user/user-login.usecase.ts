import {Observable} from 'rxjs';
import {UseCase} from '../../base/use-case';
import {UserRepository} from '../../repositories/user/user.repository';
import {TokenModel} from "../../model/token.model";

export class UserLoginUseCase implements UseCase<{ username: string; password: string }, TokenModel> {
  constructor(private userRepository: UserRepository) {
  }

  execute(
    params: { username: string, password: string },
  ): Observable<TokenModel> {
    return this.userRepository.login(params);
  }
}
