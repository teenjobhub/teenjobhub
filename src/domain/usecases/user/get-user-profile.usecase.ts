import {Observable} from 'rxjs';
import {UseCase} from '../../base/use-case';
import {UserProfileRepository} from "../../repositories/user-profile/user-profile.repository";
import {UserProfileModel} from "../../model/user-profile.model";

export class GetUserProfileUseCase implements UseCase<string, UserProfileModel> {
  constructor(private userRepository: UserProfileRepository) {
  }

  execute(userID: string): Observable<UserProfileModel> {
    return this.userRepository.getUserProfile(userID);
  }
}
