import {UseCase} from "../../base/use-case";
import {Observable} from "rxjs";
import {UserProfileModel} from "../../model/user-profile.model";
import {UserProfileRepository} from "../../repositories/user-profile/user-profile.repository";

export class SearchUsecase implements UseCase<any, UserProfileModel[]> {
    constructor(private userRepository: UserProfileRepository) {
    }

    execute(
        params: { query: string },
    ): Observable<UserProfileModel[]> {
        return this.userRepository.search(params.query);
    }
}
