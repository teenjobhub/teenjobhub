import {UseCase} from "../../base/use-case";
import {UserProfileRepository} from "../../repositories/user-profile/user-profile.repository";
import {Observable} from "rxjs";
import {UserProfileModel} from "../../model/user-profile.model";

export class UpdateUserSignatureUsecase implements UseCase<any, UserProfileModel> {
  constructor(private userRepository: UserProfileRepository) {
  }

  execute(params: { userImage: File, id: string }): Observable<UserProfileModel> {
    return this.userRepository.updateUserSignature(params.userImage, params.id);
  }
}
