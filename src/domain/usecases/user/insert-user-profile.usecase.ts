import {UseCase} from "../../base/use-case";
import {UserProfileModel} from "../../model/user-profile.model";
import {UserProfileRepository} from "../../repositories/user-profile/user-profile.repository";
import {Observable} from "rxjs";

export class InsertUserProfileUseCase implements UseCase<any, UserProfileModel> {
  constructor(private userRepository: UserProfileRepository) {
  }

  execute(params: { userProfile: UserProfileModel, userImage: File }): Observable<UserProfileModel> {
    const {userProfile, userImage} = params;
    return this.userRepository.addUserProfile(userProfile, userImage);
  }
}
