import {Observable} from 'rxjs';
import {UseCase} from '../../base/use-case';
import {UserModel} from "../../model/user.model";
import {UserRepository} from "../../repositories/user/user.repository";
import {RoleModel} from "../../model/role.model";

export class UserRegisterUseCase implements UseCase<{
  email: string,
  password: string,
  username: string,
  role: RoleModel
}, UserModel> {
  constructor(private userRepository: UserRepository) {
  }

  execute(
    params: { email: string, password: string, username: string, },
  ): Observable<UserModel> {
    return this.userRepository.register(params);
  }
}
