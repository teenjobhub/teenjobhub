import {UseCase} from "../../base/use-case";
import {ConnectionRepository} from "../../repositories/connections/connection.repository";
import {Observable} from "rxjs";

export class AcceptordenyConnectionUsecase implements UseCase<any, boolean> {
  constructor(private connectionRepository: ConnectionRepository) {
  }

  execute(param: { userDecision: boolean, connectionRequestID: string }): Observable<boolean> {
    return this.connectionRepository.acceptOrDenyConnectionRequest(param);
  }
}
