import {UseCase} from "../../base/use-case";
import {ConnectionRequestModel} from "../../model/connection-request.model";
import {ConnectionRepository} from "../../repositories/connections/connection.repository";
import {Observable} from "rxjs";
import {UserProfileModel} from "../../model/user-profile.model";

export class GetSuggestionsUsecase implements UseCase<any, UserProfileModel[]> {
    constructor(private connectionRepository: ConnectionRepository) {
    }

    execute(): Observable<UserProfileModel[]> {
        return this.connectionRepository.getSuggestions();
    }
}
