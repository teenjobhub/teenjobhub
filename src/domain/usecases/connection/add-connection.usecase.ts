import {UseCase} from "../../base/use-case";
import {ConnectionRepository} from "../../repositories/connections/connection.repository";
import {Observable} from "rxjs";
import {ConnectionRequestModel} from "../../model/connection-request.model";

export class AddConnectionUsecase implements UseCase<any, boolean> {
  constructor(private connectionRepository: ConnectionRepository) {
  }

  execute(param: { destinationUserID: string }): Observable<boolean> {
    return this.connectionRepository.addConnection(param);
  }
}
