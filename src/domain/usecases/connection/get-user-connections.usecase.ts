import {UseCase} from "../../base/use-case";
import {ConnectionRepository} from "../../repositories/connections/connection.repository";
import {Observable} from "rxjs";
import {ConnectionRequestModel} from "../../model/connection-request.model";

export class GetUserConnectionsUsecase implements UseCase<any, ConnectionRequestModel[]> {
  constructor(private connectionRepository: ConnectionRepository) {
  }

  execute(): Observable<ConnectionRequestModel[]> {
    return this.connectionRepository.getUserConnections();
  }
}
