import {UseCase} from "../../base/use-case";
import {Observable} from "rxjs";
import {ConnectionRequestModel} from "../../model/connection-request.model";
import {ConnectionRepository} from "../../repositories/connections/connection.repository";

export class GetPendingConnectionsUsecase implements UseCase<void, ConnectionRequestModel[]> {
  constructor(private connectionRepository: ConnectionRepository) {
  }

  execute(): Observable<ConnectionRequestModel[]> {
    return this.connectionRepository.getPendingConnections();
  }
}
