import {UseCase} from "../../base/use-case";
import {NotificationModel} from "../../model/notifications.model";
import {Observable} from "rxjs";
import {NotificationRepository} from "../../repositories/notification/notification.repository";

export class GetNotificationUsecase implements UseCase<any, NotificationModel[]> {
  constructor(private notificationRepository: NotificationRepository) {
  }

  execute(): Observable<NotificationModel[]> {
    return this.notificationRepository.getNotificationsByUser();
  }

}
