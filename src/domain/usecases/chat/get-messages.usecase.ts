import {UseCase} from "../../base/use-case";
import {ChatRoomModel} from "../../model/chat-room.model";
import {ChatRepository} from "../../repositories/chat/chat.repository";
import {Observable} from "rxjs";
import {ChatMessageModel} from "../../model/chat-message.model";

export class GetMessagesUsecase implements UseCase<any, ChatMessageModel[]> {
  constructor(private chatRepository: ChatRepository) {
  }

  execute( chatRoomId: string ): Observable<ChatMessageModel[]> {
    return this.chatRepository.getMessages(chatRoomId);
  }
}
