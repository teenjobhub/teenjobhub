import {UseCase} from "../../base/use-case";
import {ChatRoomModel} from "../../model/chat-room.model";
import {ChatRepository} from "../../repositories/chat/chat.repository";
import {Observable} from "rxjs";
import {ChatMessageModel} from "../../model/chat-message.model";

export class SendMessageUsecase implements UseCase<any, boolean> {
  constructor(private chatRepository: ChatRepository) {
  }

  execute( param:{chatRoomId: string,message: string} ): Observable<boolean> {
    return this.chatRepository.sendMessage(param.chatRoomId,param.message);
  }
}
