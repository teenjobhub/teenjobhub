import {UseCase} from "../../base/use-case";
import {ConnectionRepository} from "../../repositories/connections/connection.repository";
import {Observable} from "rxjs";
import {ConnectionRequestModel} from "../../model/connection-request.model";
import {ChatRepository} from "../../repositories/chat/chat.repository";
import {ChatRoomModel} from "../../model/chat-room.model";

export class GetChatRoomUsecase implements UseCase<any, ChatRoomModel> {
  constructor(private chatRepository: ChatRepository) {
  }

  execute( chatRoomOrUserProfileId: string ): Observable<ChatRoomModel> {
    return this.chatRepository.getChatRoom(chatRoomOrUserProfileId);
  }
}
