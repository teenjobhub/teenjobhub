import {UseCase} from "../../base/use-case";
import {Observable} from "rxjs";
import {ChatRepository} from "../../repositories/chat/chat.repository";
import {ChatRoomModel} from "../../model/chat-room.model";

export class GetChatRoomsUsecase implements UseCase<any, ChatRoomModel[]> {
  constructor(private chatRepository: ChatRepository) {
  }

  execute( userProfileId: string ): Observable<ChatRoomModel[]> {
    return this.chatRepository.getChatRooms(userProfileId);
  }
}
