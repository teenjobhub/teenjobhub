import {Observable} from "rxjs";
import {UseCase} from "../../base/use-case";
import {AddPostModel} from "../../model/add-post.model";
import {PostsRepository} from "../../repositories/posts/posts.repository";
import {JobPostModel} from "../../model/job-post.model";

export class AddPostUseCase implements UseCase<{ postsModel: AddPostModel }, JobPostModel> {
  constructor(private postsRepository: PostsRepository) {
  }

  execute(
    params: { postsModel: AddPostModel },
  ): Observable<JobPostModel> {
    return this.postsRepository.addPost(params);
  }
}
