import {UseCase} from "../../base/use-case";
import {PostsRepository} from "../../repositories/posts/posts.repository";
import {Observable} from "rxjs";
import {MessageModel} from "../../model/message";

export class HireUsecase implements UseCase<{ jobPostID: string, userProfileID: string }, MessageModel> {
  constructor(private postsRepository: PostsRepository) {
  }

  execute(param: { jobPostID: string, userProfileID: string }): Observable<MessageModel> {
    return this.postsRepository.hire(param.jobPostID, param.userProfileID);
  }
}
