import {UseCase} from "../../base/use-case";
import {PostsRepository} from "../../repositories/posts/posts.repository";
import {Observable} from "rxjs";
import {JobPostModel} from "../../model/job-post.model";

export class GetPostUseCase implements UseCase<void, JobPostModel[]> {
  constructor(private postsRepository: PostsRepository) {
  }

  execute(): Observable<JobPostModel[]> {
    return this.postsRepository.getPosts();
  }
}
