import {UseCase} from "../../base/use-case";
import {JobPostModel} from "../../model/job-post.model";
import {PostsRepository} from "../../repositories/posts/posts.repository";
import {Observable} from "rxjs";

export class RateUsecase implements UseCase<any, boolean> {
  constructor(private postsRepository: PostsRepository) {
  }

  execute(params:{jobPostID: string,rate:number}): Observable<boolean> {
    return this.postsRepository.rate(params.jobPostID,params.rate);
  }
}
