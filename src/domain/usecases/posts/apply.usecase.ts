import {UseCase} from "../../base/use-case";
import {PostsRepository} from "../../repositories/posts/posts.repository";
import {Observable} from "rxjs";

export class ApplyUsecase implements UseCase<string, boolean> {
  constructor(private postsRepository: PostsRepository) {
  }

  execute(jobPostID: string): Observable<boolean> {
    return this.postsRepository.apply(jobPostID);
  }
}
