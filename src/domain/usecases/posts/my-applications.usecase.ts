import {UseCase} from "../../base/use-case";
import {JobPostModel} from "../../model/job-post.model";
import {PostsRepository} from "../../repositories/posts/posts.repository";
import {Observable} from "rxjs";

export class MyApplicationsUsecase implements UseCase<any, JobPostModel[]> {
  constructor(private postsRepository: PostsRepository) {
  }

  execute(): Observable<JobPostModel[]> {
    return this.postsRepository.myApplications();
  }
}
