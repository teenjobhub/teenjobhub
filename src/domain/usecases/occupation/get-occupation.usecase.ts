import {UseCase} from "../../base/use-case";
import {Observable} from "rxjs";
import {OccupationModel} from "../../model/occupation.model";
import {OccupationRepository} from "../../repositories/occupation/occupation.repository";

export class GetOccupationUsecase implements UseCase<void, OccupationModel[]> {
  constructor(private occupationRepository: OccupationRepository) {
  }

  execute(): Observable<OccupationModel[]> {
    return this.occupationRepository.getOccupations();
  }
}
